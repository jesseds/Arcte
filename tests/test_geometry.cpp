#include <gtest/gtest.h>
#include <geometry.h>

constexpr double tol = 1e-7;

class HMB {

public:
    explicit HMB(){}

    double static constexpr a = 9.01;
    double static constexpr b = 8.926;
    double static constexpr c = 5.344;
    double static constexpr alpha = 44.45;
    double static constexpr beta = 116 + 43/60;
    double static constexpr gamma = 119 + 34/60;

    double vol = 258.4148;

    // HMB std library containers
    std::array<double, 3> static constexpr lengths {a, b, c};
    std::array<double, 3> static constexpr angles {alpha, beta, gamma};

    // Eigen containers
    Vector3d const eig_lengths = ((Vector3d() << a, b, c).finished());
    Vector3d const eig_angles = (Vector3d() << alpha, beta, gamma).finished();
};


TEST(Deg2Rad, Positive) {
    EXPECT_EQ(geom::deg2rad(0), 0);
    EXPECT_NEAR(geom::deg2rad(90.0), 3.14159265/2, tol);
    EXPECT_NEAR(geom::deg2rad(180.0), 3.14159265, tol);
    EXPECT_NEAR(geom::deg2rad(270.0), 3.14159265*1.5, tol);
    EXPECT_NEAR(geom::deg2rad(360.0), 2*3.14159265, tol);
}


TEST(Deg2Rad, Negative) {
    EXPECT_NEAR(geom::deg2rad(-90.0), -3.14159265/2, tol);
    EXPECT_NEAR(geom::deg2rad(-180.0), -3.14159265, tol);
    EXPECT_NEAR(geom::deg2rad(-270.0), -1.5*3.14159265, tol);
    EXPECT_NEAR(geom::deg2rad(-360.0), -2*3.14159265, tol);
}


TEST(Rad2Deg, Positive) {
    EXPECT_NEAR(geom::rad2deg(0.0), 0.0, tol);
    EXPECT_NEAR(geom::rad2deg(geom::pi/2), 90.0, tol);
    EXPECT_NEAR(geom::rad2deg(geom::pi), 180.0, tol);
    EXPECT_NEAR(geom::rad2deg(geom::pi*1.5), 270, tol);
    EXPECT_NEAR(geom::rad2deg(geom::pi*2), 360, tol);
}


TEST(Rad2Deg, Negative) {
    EXPECT_NEAR(geom::rad2deg(-geom::pi/2), -90.0, tol);
    EXPECT_NEAR(geom::rad2deg(-geom::pi), -180.0, tol);
    EXPECT_NEAR(geom::rad2deg(-geom::pi*1.5), -270, tol);
    EXPECT_NEAR(geom::rad2deg(-geom::pi*2), -360, tol);
}


TEST(RoundFloor, Positive) {
    EXPECT_NEAR(geom::roundFloor(3.14134), 3.141, tol);
    EXPECT_NEAR(geom::roundFloor(12.75, 1), 12.7, tol);
    EXPECT_NEAR(geom::roundFloor(12.75, 0), 12.0, tol);
    EXPECT_NEAR(geom::roundFloor(0.3, 0), 0, tol);
}

TEST(RoundFloor, Negative) {
    EXPECT_NEAR(geom::roundFloor(-45.3176, 0), -46, tol);
    EXPECT_NEAR(geom::roundFloor(-45.3176, 1), -45.4, tol);
    EXPECT_NEAR(geom::roundFloor(-45.3176, 3), -45.318, tol);
}
