/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include <QWidget>
#include <document.h>
#include <QStandardItemModel>


namespace Ui {
class BondEditor;
}

class BondEditor : public QWidget {
    Q_OBJECT

private:
    Ui::BondEditor *ui;
    Document* doc;
    QStandardItemModel* model;

    QList<QStandardItem*> makeBondItems(Bond const& bond);

public:
    explicit BondEditor(QWidget* parent, Document* document);
    ~BondEditor();

    std::vector<QAction*> toolbarActions();

private slots:
    void onItemChanged(QStandardItem* item);
    void onBondAdded(Bond* bond);
    void onBondsCleared();
    void onBondRemovedId(unsigned int);

    void on_newbond_btn_clicked();
    void on_clear_btn_clicked();
    void on_del_selected_btn_clicked();

    void on_auto_btn_clicked();

signals:
    void emitReconstructView();

};
