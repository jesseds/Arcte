/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include <QDialog>
#include <QTreeView>
#include <QVBoxLayout>
#include <QDialogButtonBox>
#include <document.h>
#include <QResource>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <gemmi/cif.hpp>


// Must use subclass of QResources to get list of cif paths since QResource::children is protected
class QCifsResource : public QResource {

private:
    QString const prefix;

public:
    explicit QCifsResource(QString const& prefix);

    QStringList cifList();
};


class StructFilterProxyModel : public QSortFilterProxyModel {
    Q_OBJECT

public:
    explicit StructFilterProxyModel(QWidget* parent) : QSortFilterProxyModel(parent) {}

protected:
    bool virtual filterAcceptsRow(int source_row, QModelIndex const& source_parent) const override;
};


class BundledStructDialog : public QDialog {
    Q_OBJECT

private:
    QTreeView* table;
    QStandardItemModel* model;
    StructFilterProxyModel* model_prox;
    QVBoxLayout* layout;
    QDialogButtonBox* btns;
    QLineEdit* filter;

    QStandardItem* elements;
    QStandardItem* ab;
    QStandardItem* ab2;
    QStandardItem* ambn;
    QStandardItem* other_compounds;

    void importStructures();
    QList<QStandardItem*> makeStructureRow(Document const& doc, QString const& cif_url);
    QStandardItem* makeSectionRowItem(QString name);

public:
    explicit BundledStructDialog(QWidget* parent);

    std::unique_ptr<gemmi::cif::Document> selected_doc;

private slots:
    void onDoubleClicked(QModelIndex const& idx);

public slots:
    void accept();

signals:
    void structureAccepted(QFileInfo);
};
