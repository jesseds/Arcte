/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once
#include <QDialog>
#include <scenewidget.h>
#include <vtkCaptionActor2D.h>

namespace Ui {
class MeasureBondAngleDialog;
}

class MeasureBondAngleDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MeasureBondAngleDialog(QWidget *parent, Document* doc, SceneWidget* scene);
    ~MeasureBondAngleDialog();

    void reset();

private slots:
    void on_select_btn_clicked();
    void actor_pick_received(unsigned int basis_id, double x, double y, double z);

public slots:
    void closeEvent(QCloseEvent *) override;

private:
    void updateText();
    void removeActors();

    Ui::MeasureBondAngleDialog *ui;
    SceneWidget* scene;
    Document* doc;
    BasisPos* atom1_basis {nullptr};
    BasisPos* atom2_basis {nullptr};
    BasisPos* atom3_basis {nullptr};
    std::array<double, 3> atom1_xyz {0, 0, 0};
    std::array<double, 3> atom2_xyz {0, 0, 0};
    std::array<double, 3> atom3_xyz {0, 0, 0};
    bool currently_selecting {false};

    vtkSmartPointer<vtkCaptionActor2D> p1_text;
    vtkSmartPointer<vtkCaptionActor2D> p2_text;
    vtkSmartPointer<vtkCaptionActor2D> p3_text;
    vtkSmartPointer<ArcteActor> line1;
    vtkSmartPointer<ArcteActor> line2;
};

