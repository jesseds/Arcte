/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "newbonddialog.h"
#include "ui_newbonddialog.h"
#include <ciso646>

NewBondDialog::NewBondDialog(QWidget* parent, Document* doc) :
    QDialog(parent),
    ui(new Ui::NewBondDialog) {
    ui->setupUi(this);
    this->doc = doc;

    connect(ui->btn_box, &QDialogButtonBox::accepted, this, &NewBondDialog::accepted);
    connect(ui->btn_box, &QDialogButtonBox::rejected, this, &NewBondDialog::rejected);

    for (auto& elem: doc->elements()) {
        ui->elem1_edit->addItem(QString::fromStdString(elem->name));
        ui->elem2_edit->addItem(QString::fromStdString(elem->name));
    }

    onAnyElementChanged();
}


NewBondDialog::~NewBondDialog() {
    delete ui;
}


void NewBondDialog::rejected() {
    this->close();
}


void NewBondDialog::accepted() {
    Bond newbond {};
    chem::Element const* elem1 = chem::table.findByName(ui->elem1_edit->currentText().toStdString());
    chem::Element const* elem2 = chem::table.findByName(ui->elem2_edit->currentText().toStdString());
    newbond.setElem1(elem1);
    newbond.setElem2(elem2);
    newbond.setMaxRadius(ui->max_rad->value());
    newbond.setMinRadius(ui->min_rad->value());
    doc->addBond(newbond);

    this->close();
}

void NewBondDialog::onAnyElementChanged() {
    std::cout << "1" << std::endl;
    if (ui->elem1_edit->count() == 0 or ui->elem2_edit->count() == 0) {
        return;
    }
    chem::Element const* elem1 = chem::table.findByName(ui->elem1_edit->currentText().toStdString());
    chem::Element const* elem2 = chem::table.findByName(ui->elem2_edit->currentText().toStdString());
    std::array<double, 2> minmax = doc->predictBondingRadius(elem1, elem2);
    ui->min_rad->setValue(0.01);
    ui->max_rad->setValue(minmax.at(1));
}

void NewBondDialog::on_elem1_edit_currentIndexChanged(int index) {
    onAnyElementChanged();
}

void NewBondDialog::on_elem2_edit_currentIndexChanged(int index) {
    onAnyElementChanged();
}
