/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include <QDialog>
#include <QDialogButtonBox>
#include <QTreeView>
#include <document.h>
#include <QStandardItemModel>
#include <elements.h>
#include <QList>
#include <elementcolordelegate.h>
#include <elementcoloroverridedelegate.h>
#include <elementcoloreditdelegate.h>


class BasisColorEditor : public QDialog {
    Q_OBJECT

private:
    QDialogButtonBox* btns;
    QTreeView* tree;
    QStandardItemModel* model;
    Document* doc;
    void setupTree();
    QList<QStandardItem*> makeRowItems(chem::Element const * elem);
    ElementColorDelegate* default_col_delg;
    ElementColorOverrideDelegate* override_col_delg;
    ElementColorEditDelegate* edit_col_delg;
    QSettings* settings;
    static bool currently_open;

    void onDialogButtonsClicked(QAbstractButton*);

private slots:
    void onColorsBtnChanged(){emit colorsChanged();}
    void onColorsChanged();

signals:
    void colorsChanged();

public:
    explicit BasisColorEditor(QWidget* parent, Document* doc);
};

