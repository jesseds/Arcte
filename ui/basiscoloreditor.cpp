/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "basiscoloreditor.h"

#include <QVBoxLayout>
#include <QString>
#include <elementcoloreditbutton.h>
#include <mainwindow.h>
#include <QPushButton>

BasisColorEditor::BasisColorEditor(QWidget* parent, Document* doc)
    : QDialog(parent) {
    settings = new QSettings();

    setWindowTitle("Elemental colors");
    resize(450, 800);

    this->doc = doc;
    QVBoxLayout* layout = new QVBoxLayout(this);
    setLayout(layout);

    tree = new QTreeView(this);
    model = new QStandardItemModel();
    tree->setModel(model);
    layout->addWidget(tree);
    setupTree();

    default_col_delg = new ElementColorDelegate();
    override_col_delg = new ElementColorOverrideDelegate();
    edit_col_delg = new ElementColorEditDelegate();
    tree->setItemDelegateForColumn(1, default_col_delg);
    tree->setItemDelegateForColumn(2, override_col_delg);

    connect(this, &BasisColorEditor::rejected, this, &BasisEditor::close);

    btns = new QDialogButtonBox(this);
    btns->addButton(QDialogButtonBox::Close);
    btns->addButton(QDialogButtonBox::Reset);
    layout->addWidget(btns);

    connect(btns, &QDialogButtonBox::clicked, this, &BasisColorEditor::onDialogButtonsClicked);
    connect(this, &BasisColorEditor::colorsChanged, this, &BasisColorEditor::onColorsChanged);
}

void BasisColorEditor::setupTree() {
    model->setHorizontalHeaderLabels({"Element", "Default", "Override", "Edit"});
    tree->setAlternatingRowColors(true);
    tree->setRootIsDecorated(false);

    for (auto& elem: chem::table) {
        if (elem->number == 0) {
            continue;
        }
        auto items = makeRowItems(elem.get());
        model->appendRow(items);

        // Add the button for editing here
        ElementColorEditButton* editbtn = new ElementColorEditButton(this, elem->number);

        connect(editbtn, &ElementColorEditButton::colorsModified, this, &BasisColorEditor::onColorsBtnChanged);
        tree->setIndexWidget(items[3]->index(), editbtn);
    }
    tree->sortByColumn(0, Qt::SortOrder::AscendingOrder);
}

QList<QStandardItem*> BasisColorEditor::makeRowItems(chem::Element const* elem) {
    if (elem == nullptr) {
        throw std::runtime_error("Received nullptr for basiscoloreditor items");
    }
    QList<QStandardItem*> retn {};

    QStandardItem* label = new QStandardItem();
    label->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    label->setData(elem->number, Qt::UserRole);
    label->setData(QString::fromStdString(elem->name), Qt::DisplayRole);

    QStandardItem* default_col = new QStandardItem();
    default_col->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    default_col->setData(elem->number, Qt::UserRole);

    QStandardItem* override = new QStandardItem();
    override->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    override->setData(elem->number, Qt::UserRole);

    QStandardItem* edit = new QStandardItem();
    edit->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    edit->setData(elem->number, Qt::UserRole);

    retn << label << default_col << override << edit;
    return retn;
}

void BasisColorEditor::onDialogButtonsClicked(QAbstractButton* btn) {
    if (btn == btns->button(QDialogButtonBox::Reset)) {
        settings->remove("elem_color_override");
        emit colorsChanged();
    }
    else if (btn == btns->button(QDialogButtonBox::Close)) {
        reject();
    }
}

void BasisColorEditor::onColorsChanged() {
    tree->update();
}
