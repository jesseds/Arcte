/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "elementcoloroverridedelegate.h"
#include <ciso646>
#include <elements.h>
#include <QColor>
#include <QPainter>

ElementColorOverrideDelegate::ElementColorOverrideDelegate() : QStyledItemDelegate() {
    settings = new QSettings();
}

void ElementColorOverrideDelegate::paint(QPainter* painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    QStyledItemDelegate::paint(painter, option, index);

    unsigned int curr_elem = index.data(Qt::UserRole).toUInt();

    // Only paint a color indicator if it has been overriden by the user
    std::string set_str = "elem_color_override/" + std::to_string(curr_elem);
    if (! settings->contains(QString::fromStdString(set_str))) {
        QStyledItemDelegate::paint(painter, option, index);
        return;
    }
    QVariant col_var = settings->value(QString::fromStdString(set_str));
    QColor col = col_var.value<QColor>();

    if (! col.isValid()) {
        throw std::runtime_error("ElementColorOverrideDelegate: invalid color");
    }

    chem::Element const* elem_item = chem::table.findByNumber(curr_elem);
    QStyleOptionViewItem opts = option;
    QStyledItemDelegate::initStyleOption(&opts, index);
    painter->save();

    int padding = static_cast<int>(opts.rect.height()*0.35);

    QRect circ_rect {};
    circ_rect.setLeft(opts.rect.left() + padding/2);
    circ_rect.setRight(opts.rect.right() - padding/2);
    circ_rect.setTop(opts.rect.top() + padding/2);
    circ_rect.setBottom(opts.rect.bottom() - padding/2);

    QBrush fill {};
    fill.setColor(col);
    fill.setStyle(Qt::BrushStyle::SolidPattern);
    painter->setBrush(fill);
    painter->setRenderHint(QPainter::Antialiasing);
    painter->drawRect(circ_rect);
    painter->restore();
//    }
}
