/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "imageexportdialog.h"
#include "ui_imageexportdialog.h"
#include <QFileDialog>
#include <QMessageBox>
#include <vtkPNGWriter.h>
#include <QStandardPaths>
#include <QClipboard>

ImageExportDialog::ImageExportDialog(QWidget* parent, SceneWidget* scene) :
    QDialog(parent),
    ui(new Ui::ImageExportDialog) {
    ui->setupUi(this);
    this->scene = scene;
}

ImageExportDialog::~ImageExportDialog() {
    delete ui;
}

void ImageExportDialog::on_path_btn_clicked() {
    auto filepath = QFileDialog::getSaveFileName(this, "Export image path", "~/", "Portable Network Graphcs (*.png);;");
    ui->path_edit->setText(QFileInfo{filepath}.absoluteFilePath());
}


bool ImageExportDialog::prepareImageFilter() {
    QFileInfo filepath = currentFilePath();
    if (! filepath.absoluteDir().exists()) {
        QMessageBox::critical(this, "Cannot save image", "The specified path directory does not exist");
        return false;
    }
    if (ui->transparency->isChecked()) {
        img_filter->SetInputBufferTypeToRGBA();
    }
    else {
        img_filter->SetInputBufferTypeToRGB();
    }
    img_filter->SetScale(1);
    img_filter->SetInput(scene->renderer()->GetRenderWindow());
    img_filter->ReadFrontBufferOff();
    img_filter->Update();
    return true;
}


void ImageExportDialog::accept() {
    QFileInfo filepath = currentFilePath();
    if (filepath.absoluteFilePath() == "") {
        return;
    }
    prepareImageFilter();
    auto writer = vtkSmartPointer<vtkPNGWriter>::New();
    writer->SetFileName(filepath.absoluteFilePath().toStdString().data());
    writer->SetInputConnection(img_filter->GetOutputPort());
    try {
        writer->Write();
    }
    catch (std::exception) {
        QMessageBox::critical(this, "Could not save image", "Failed to save image for unknown reason.");
    }

    QDialog::accept();
}


QFileInfo ImageExportDialog::currentFilePath() {
    return QFileInfo(ui->path_edit->text());
}


void ImageExportDialog::on_copy_btn_clicked() {
    QString path = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
    QDir* fdir = new QDir(path);
    QString fullpath = fdir->filePath("arcte_temp_copy_image.png");
    QFile* file_path = new QFile(fullpath);

    prepareImageFilter();
    auto writer = vtkSmartPointer<vtkPNGWriter>::New();
    writer->SetFileName(fullpath.toStdString().data());
    writer->SetInputConnection(img_filter->GetOutputPort());
    try {
        writer->Write();
    }
    catch (std::exception) {
        QMessageBox::critical(this, "Could not save image", "Failed to save image for unknown reason.");
    }

    QImage img {fullpath};
    QClipboard* clip = QApplication::clipboard();
    clip->setImage(img);
    file_path->remove();
}
