/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include <QWidget>
#include <document.h>
#include <QStandardItemModel>
#include <QTreeView>
#include <basiscoloreditor.h>

namespace Ui {
class BasisEditor;
}

class BasisEditor : public QWidget {
    Q_OBJECT

signals:
    void emitReconstructView();

private:
    Ui::BasisEditor *ui;
    QStandardItemModel* model;
    Document* doc;
    BasisColorEditor* color_dialog;

    QStandardItem* makeElementItem(chem::Element const* elem);
    QList<QStandardItem*> makeBasisItems(BasisPos const& pos);
    QStandardItem* findElementItem(chem::Element const* elem);

public:
    explicit BasisEditor(QWidget* parent, Document* doc);
    ~BasisEditor();

    std::vector<chem::Element const*> elements();
    std::vector<QAction*> toolbarActions();

private slots:
    void on_modify_btn_clicked();
    void onBasisPosAdded(BasisPos* newpos);
    void onBasisPosRemoved(unsigned int id);
    void onElementRemoved(chem::Element const*);
    void onItemChanged(QStandardItem* item);
    void on_clear_btn_clicked();
    void onBasesCleared();
    void on_newpos_btn_clicked();

    void on_del_selected_btn_clicked();
    void on_colors_btn_clicked();
    void onColorEditorModified();
};
