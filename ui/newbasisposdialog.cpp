/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "newbasisposdialog.h"
#include "ui_newbasisposdialog.h"

NewBasisPosDialog::NewBasisPosDialog(BasisEditor *parent, Document* doc) :
    QDialog(parent),
    ui(new Ui::NewBasisPosDialog) {
    ui->setupUi(this);
    this->doc = doc;

    connect(ui->btn_box, &QDialogButtonBox::accepted, this, &NewBasisPosDialog::accepted);
    connect(ui->btn_box, &QDialogButtonBox::rejected, this, &NewBasisPosDialog::rejected);

    for (auto& elem: parent->elements()){
        ui->elem_edit->addItem(QString::fromStdString(elem->name));
    }
}


NewBasisPosDialog::~NewBasisPosDialog(){
    delete ui;
}


void NewBasisPosDialog::rejected(){
    this->close();
}


void NewBasisPosDialog::accepted(){
    BasisPos newbasis {};
    std::string elem_str = ui->elem_edit->currentText().toStdString();
    auto species = chem::table.findByName(elem_str);
    newbasis.setX(ui->x_edit->value());
    newbasis.setY(ui->y_edit->value());
    newbasis.setZ(ui->z_edit->value());

    newbasis.setSpecies(species);
    newbasis.setRadiusType(doc->radiusType());
    doc->addBasisPos(newbasis);
    this->close();
}
