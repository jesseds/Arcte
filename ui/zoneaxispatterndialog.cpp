/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "zoneaxispatterndialog.h"
#include "ui_zoneaxispatterndialog.h"
#include <scatteringfactors.h>
#include <geometry.h>
#include <QMessageBox>
#include <ciso646>
#include <FramelessHelper/Widgets/framelessmainwindow.h>
#include <FramelessHelper/Widgets/framelesswidgetshelper.h>
#include <FramelessHelper/Widgets/standardtitlebar.h>
#include <framelesswidgetshelper.h>
#include <FramelessHelper/Widgets/standardsystembutton.h>
#include <FramelessHelper/Core/utils.h>
#include <QMenuBar>
#include <frameless.h>

using namespace wangwenx190;

ZoneAxisPatternDialog::ZoneAxisPatternDialog(QWidget *parent, Document const* doc) :
    QMainWindow(parent), ui(new Ui::ZoneAxisPatternDialog),
    doc(doc) {
    ui->setupUi(this);








//    titlebar_widget = new QMenuBar(this);



//    std::cout << "Runn" << "\n";
//    #ifdef WIN32

//        auto fw_helper = FramelessHelper::FramelessWidgetsHelper::get(this);
//        fw_helper->extendsContentIntoTitleBar(true);
//        m_titleBar = new FramelessHelper::StandardTitleBar(this);
//        m_titleBar->setWindowIconVisible(true);

//        QColor background {};
//        background = QColor(245, 245, 245);
//        m_titleBar->chromePalette()->setTitleBarActiveBackgroundColor(background);
//        m_titleBar->chromePalette()->setTitleBarInactiveBackgroundColor(background);

//        m_titleBar->setTitleLabelAlignment(Qt::AlignCenter);
//        fw_helper->setSystemButton(m_titleBar->closeButton(), FramelessHelper::Global::SystemButtonType::Close);
//        fw_helper->setSystemButton(m_titleBar->minimizeButton(), FramelessHelper::Global::SystemButtonType::Minimize);
//        fw_helper->setSystemButton(m_titleBar->maximizeButton(), FramelessHelper::Global::SystemButtonType::Maximize);

////        auto spacer = new QWidget(this);
////        spacer->setFixedWidth(35);

////        const auto titleBarLayout = static_cast<QHBoxLayout *>(m_titleBar->layout());
////        titleBarLayout->insertWidget(0, titlebar_widget);
////        titleBarLayout->insertWidget(0, spacer);
////        titlebar_widget->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);

////        auto mainwindow = dynamic_cast<QMainWindow*>(parent);
////        if (mainwindow) {
////            mainwindow->setMenuWidget(m_titleBar);
////        }

//        fw_helper->setTitleBarWidget(m_titleBar);

//        fw_helper->setHitTestVisible(titlebar_widget);
////        fw_helper->setHitTestVisible(spacer);
//        setMenuWidget(m_titleBar);
//    #endif



























//    titlebar_widget = new QMenuBar(this);
//    auto titlebar = setupTitleBar(this, titlebar_widget);
////    FramelessHelper::FramelessWidgetsHelper::get(this)->waitForReady();
//    if (titlebar) {
//        this->layout()->setMenuBar(titlebar.value());
//        setMenuWidget(titlebar.value());
//    }

    QVBoxLayout* plot_layout = new QVBoxLayout();
    ediff_sim = std::make_unique<ediff::ZoneAxisPattern>(doc);
    plotter = new ZoneAxisPlotter(this, ediff_sim.get());
    plot_layout->addWidget(plotter);
    plot_layout->setContentsMargins(0, 0, 0, 0);
    ui->tab_pattern->setLayout(plot_layout);
    ui->dockWidget->setTitleBarWidget(new QWidget());
    resize(1200, 600);


    _hover_label = new QLabel();
    ui->statusBar->addWidget(_hover_label);

//    _last_magnification = ui->magn_slider->value();

    ui->conv_edit->setMaximum(0.5);
    ui->conv_edit->setMinimum(0);

    // Custom scattering factors menu selection
    _sfs_select = new sfs::QScatteringFactorsElectronCombo(this);
    _sfs_select->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
    auto prev = ui->parameters_tab->layout()->replaceWidget(ui->sfs_placeholder, _sfs_select);
    delete prev;

    // Custom scattering factors menu selection
    _model_select = new ediff::QModelCombo(this);
    _model_select->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
    auto prev2 = ui->parameters_tab->layout()->replaceWidget(ui->model_placeholder, _model_select);
    delete prev2;

    // Connections from pattern simulation
    connect(ediff_sim.get(), &ediff::ZoneAxisPattern::maxGChanged, ui->max_g_edit, &QDoubleSpinBox::setValue);
    connect(ediff_sim.get(), &ediff::ZoneAxisPattern::maxSChanged, ui->max_s_edit, &QDoubleSpinBox::setValue);
    connect(ediff_sim.get(), &ediff::ZoneAxisPattern::maxLaueZoneChanged, ui->max_laue_edit, &QSpinBox::setValue);
    connect(ediff_sim.get(), &ediff::ZoneAxisPattern::voltageChanged, ui->voltage_edit, &QDoubleSpinBox::setValue);
    connect(ediff_sim.get(), &ediff::ZoneAxisPattern::zoneAxisChanged, this, &ZoneAxisPatternDialog::onSimZoneAxisChanged);
    connect(ediff_sim.get(), &ediff::ZoneAxisPattern::calculationFinished, this, &ZoneAxisPatternDialog::onCalculationFinished);
    connect(ediff_sim.get(), &ediff::ZoneAxisPattern::sfParamChanged, _sfs_select, &sfs::QScatteringFactorsElectronCombo::setParam);
    connect(ediff_sim.get(), &ediff::ZoneAxisPattern::modelChanged, _model_select, &ediff::QModelCombo::setModel);
    connect(ediff_sim.get(), &ediff::ZoneAxisPattern::thicknessChanged, ui->thickness_edit, &QDoubleSpinBox::setValue);

    // Connections from plotter
    connect(plotter, &ZoneAxisPlotter::convSemiAngleChanged, ui->conv_edit, &QDoubleSliderEdit::setValue);
    connect(plotter, &ZoneAxisPlotter::gammaChanged, ui->min_int_edit, &QDoubleSliderEdit::setValue);
    connect(plotter, &ZoneAxisPlotter::showSpotLabelsChanged, ui->show_hkl_labels, &QCheckBox::setChecked);
//    connect(plotter, &ZoneAxisPlotter::showLaueCirclesChanged, ui->show_laue_circles, &QCheckBox::setChecked);
    connect(plotter, &ZoneAxisPlotter::showKikuchiLinesChanged, ui->show_kikuchi_lines, &QCheckBox::setChecked);
    connect(plotter, &ZoneAxisPlotter::rotationChanged, ui->rotation_edit, &QDoubleSliderEdit::setValue);
    connect(plotter, &ZoneAxisPlotter::kikuchiThresholdChanged, ui->kikuchi_thresh, &QDoubleSliderEdit::setValue);
    connect(plotter, &ZoneAxisPlotter::hklLabelThresholdChanged, ui->hkl_label_thresh, &QDoubleSliderEdit::setValue);
    connect(plotter, &ZoneAxisPlotter::hoverTextChanged, _hover_label, &QLabel::setText);

    // Connections from here to plotter
    connect(ui->conv_edit, qOverload<double>(&QDoubleSliderEdit::valueChanged), plotter, &ZoneAxisPlotter::setConvSemiAngle);
    connect(ui->min_int_edit, qOverload<double>(&QDoubleSliderEdit::valueChanged), plotter, &ZoneAxisPlotter::setGamma);
    connect(ui->rotation_edit, qOverload<double>(&QDoubleSliderEdit::valueChanged), plotter, &ZoneAxisPlotter::setRotation);
    connect(ui->hkl_label_thresh, qOverload<double>(&QDoubleSliderEdit::valueChanged), plotter, &ZoneAxisPlotter::setHklLabelThreshold);
    connect(ui->kikuchi_thresh, qOverload<double>(&QDoubleSliderEdit::valueChanged), plotter, &ZoneAxisPlotter::setKikuchiThreshold);
    connect(ui->show_hkl_labels, &QCheckBox::toggled, plotter, &ZoneAxisPlotter::setShowSpotLabels);
//    connect(ui->show_laue_circles, &QCheckBox::toggled, plotter, &ZoneAxisPlotter::setShowLaueCircles);
    connect(ui->show_kikuchi_lines, &QCheckBox::toggled, plotter, &ZoneAxisPlotter::setShowKikuchiLines);

    // Initialize properties of plotter and simulation now that connections are made
    ediff_sim->initialize();
    plotter->initializeProperties();

    // Setup reflections table
    table = new QTabulate(this, ediff_sim->refl_table());
    auto refl_layout = new QVBoxLayout();
    refl_layout->setContentsMargins(0, 0, 0, 0);

    ui->tab_reflections->setLayout(refl_layout);
    refl_layout->addWidget(table);

    addToolBar(plotter->toolbar);
    addToolBar(table->toolbar);

    // Keep window on top like a dialog
    setWindowFlag(Qt::Dialog, true);
}

void ZoneAxisPatternDialog::setHoverStatus(QString const& text) {
    _hover_label->setText(text);
}

void ZoneAxisPatternDialog::show() {
    ediff_sim->calculate();
    QMainWindow::show();
}

ZoneAxisPatternDialog::~ZoneAxisPatternDialog() {
    delete ui;
}

void ZoneAxisPatternDialog::on_calculate_btn_clicked() {
    // Check if scattering factors are tabulated for the elements in the document
    auto factors = sfs::factorsFromEnum(_sfs_select->currentParam());
    for (auto const& basis: doc->basisPositions()) {
        if (not factors->exists(basis->species())) {
            QMessageBox::information(this, "Elemental species not available", "Scattering factors are not available for one or more elements.");
            return;
        }
    }

    // Guard against calculating the 0 0 0 zone axis
    if (ui->h_edit->value() == 0 and ui->k_edit->value() == 0 and ui->l_edit->value() == 0) {

        ediff_sim->setZoneAxis(1, 0, 0);
        ui->h_edit->setValue(1);
    }

    Vector3i zoneaxis {};
    zoneaxis << ui->h_edit->value(), ui->k_edit->value(), ui->l_edit->value();
    ediff_sim->setZoneAxis(zoneaxis);
    ediff_sim->setVoltage(ui->voltage_edit->value());
    ediff_sim->setMaxLaueZone(ui->max_laue_edit->value());
    ediff_sim->setMaxG(ui->max_g_edit->value());
    ediff_sim->setMaxS(ui->max_s_edit->value());
    ediff_sim->setSfParam(_sfs_select->currentParam());
    ediff_sim->setModel(_model_select->currModel());
    ediff_sim->setThickness(ui->thickness_edit->value());

    ediff_sim->calculate();
}


void ZoneAxisPatternDialog::onSimZoneAxisChanged(int h, int k, int l) {
    ui->h_edit->setValue(h);
    ui->k_edit->setValue(k);
    ui->l_edit->setValue(l);
}


void ZoneAxisPatternDialog::onCalculationFinished() {
    plotter->rebuild();
}

void ZoneAxisPatternDialog::on_magn_slider_valueChanged(double value) {
    plotter->setCameraLength(value);
//    int count = std::abs(value - _last_magnification);
//    if (value > _last_magnification) {
//        plotter->zoomIn(count);
//    }
//    else {
//        plotter->zoomOut(count);
//    }
//    _last_magnification = value;
}

