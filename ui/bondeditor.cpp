/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "bondeditor.h"
#include "ui_bondeditor.h"
#include <newbonddialog.h>
#include <QMessageBox>
#include <QAction>
#include <sstream>

BondEditor::BondEditor(QWidget* parent, Document* document) :
    QWidget(parent),
    ui(new Ui::BondEditor) {
    ui->setupUi(this);

    this->doc = document;

    model = new QStandardItemModel();
    ui->tree->setModel(model);
    ui->tree->setAlternatingRowColors(true);
    model->setHorizontalHeaderLabels({"Elem 1", "Elem 2", "Min", "Max"});
    ui->tree->header()->resizeSection(0, 100);
    ui->tree->header()->resizeSection(1, 100);
    ui->tree->header()->resizeSection(2, 60);
    ui->tree->header()->resizeSection(3, 60);

    connect(doc, &Document::bondAdded, this, &BondEditor::onBondAdded);
    connect(doc, &Document::bondsCleared, this, &BondEditor::onBondsCleared);
    connect(doc, &Document::bondRemovedId, this, &BondEditor::onBondRemovedId);
    connect(this->model, &QStandardItemModel::itemChanged, this, &BondEditor::onItemChanged);

}

BondEditor::~BondEditor() {
    delete ui;
}


void BondEditor::onBondAdded(Bond* bond) {
    auto items = makeBondItems(*bond);

    model->appendRow(items);
}

std::vector<QAction*> BondEditor::toolbarActions() {
    std::vector<QAction*> retn {};
    QAction* edit = new QAction(ui->newbond_btn->icon(), ui->newbond_btn->toolTip(), this);
    connect(edit, &QAction::triggered, ui->newbond_btn, &QToolButton::clicked);
    retn.push_back(edit);
    return retn;
}

QList<QStandardItem*> BondEditor::makeBondItems(Bond const& bond) {
    QList<QStandardItem*> retn {};

    std::stringstream min_ss {};
    min_ss << std::fixed << std::setprecision(3) <<  bond.minRadius();
    std::stringstream max_ss {};
    max_ss << std::fixed << std::setprecision(3) <<  bond.maxRadius();

    QStandardItem* item_lbl1 = new QStandardItem();
    item_lbl1->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    item_lbl1->setData(bond.id(), Qt::UserRole);
    item_lbl1->setData(QString::fromStdString(bond.elem1()->name), Qt::EditRole);

    QStandardItem* item_lbl2 = new QStandardItem();
    item_lbl2->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    item_lbl2->setData(bond.id(), Qt::UserRole);
    item_lbl2->setData(QString::fromStdString(bond.elem2()->name), Qt::EditRole);

    QStandardItem* item_min = new QStandardItem();
    item_min->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable);
    item_min->setData(bond.id(), Qt::UserRole);
    item_min->setData(QString::fromStdString(min_ss.str()), Qt::EditRole);

    QStandardItem* item_max = new QStandardItem();
    item_max->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable);
    item_max->setData(bond.id(), Qt::UserRole);
    item_max->setData(QString::fromStdString(max_ss.str()), Qt::EditRole);

    retn << item_lbl1 << item_lbl2 << item_min << item_max;

    return retn;
}


void BondEditor::onBondsCleared() {
    model->removeRows(0, model->rowCount());
}


void BondEditor::onItemChanged(QStandardItem* item) {
    auto id = item->data(Qt::UserRole).toUInt();
    Bond* bond = doc->findBondById(id);
    auto col = item->column();

    switch (col) {
    case 2: {
        double rad = std::stod(item->data(Qt::DisplayRole).toString().toStdString());
        if (bond->minRadius() == rad) {
            break;
        }

        try {
            bond->setMinRadius(rad);
            emit emitReconstructView();
        }
        catch (std::exception) {
            std::string prev_rad = std::to_string(bond->minRadius());
            item->setData(bond->minRadius(), Qt::DisplayRole);
        }
        break;
    }
    case 3: {
        double rad = std::stod(item->data(Qt::DisplayRole).toString().toStdString());
        if (bond->maxRadius() == rad) {
            break;
        }

        try {
            bond->setMaxRadius(rad);
            emit emitReconstructView();
        }
        catch (std::exception) {
            std::string prev_rad = std::to_string(bond->maxRadius());
            item->setData(bond->maxRadius(), Qt::DisplayRole);
        }
        break;
    }
    }
}


void BondEditor::on_newbond_btn_clicked() {
    if (doc->nPositions() == 0) {
        QMessageBox::warning(this, "No basis positions", "Cannot add bonds with no basis positions");
        return;
    }
    NewBondDialog* win = new NewBondDialog(this, doc);
    win->exec();
}

void BondEditor::on_clear_btn_clicked() {
    doc->clearBonds();
}


void BondEditor::onBondRemovedId(unsigned int id) {
    for (int i=0; i<model->rowCount(); ++i) {
        QModelIndex idx = model->index(i, 0, QModelIndex());
        QStandardItem* item = model->itemFromIndex(idx);
        if (item->data(Qt::UserRole).toUInt() == id) {
            model->removeRow(idx.row(), QModelIndex());
            return;
        }
    }
}


void BondEditor::on_del_selected_btn_clicked() {
    doc->suppress_view_update = true;
    auto selected = ui->tree->selectionModel()->selectedIndexes();

    // Do nothing if nothing is selected
    if (selected.size() == 0){
        return;
    }

    for (auto& i: selected){
        // Since we only want to operate once per row, we will skip any item whos col is not 0
        if (i.column() != 0){
            continue;
        }
        auto id = i.data(Qt::UserRole).toUInt();
        doc->removeBondById(id);
    }

    doc->suppress_view_update = false;
    emit emitReconstructView();

}

void BondEditor::on_auto_btn_clicked() {
    // TODO: we must suppress the view update or bonds with no connections will be automatically removed in
    // a way that the basis editor does not expect and segfault will occur, should fix
    doc->suppress_view_update = true;
    doc->predictBonding();
    doc->suppress_view_update = false;
    emit emitReconstructView();
}

