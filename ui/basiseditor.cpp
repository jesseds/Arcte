/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "basiseditor.h"
#include "ui_basiseditor.h"
#include <QStringList>
#include <QVariant>
#include <sstream>
#include <chooseelementdialog.h>
#include <newbasisposdialog.h>
#include <QMessageBox>
#include <QDoubleSpinBox>
#include <basiscoloreditor.h>
#include <QAction>
#include <basisposdelegate.h>
#include <basisnamedelegate.h>

BasisEditor::BasisEditor(QWidget* parent, Document* doc) :
    QWidget(parent),
    ui(new Ui::BasisEditor){
    ui->setupUi(this);

    this->doc = doc;
    color_dialog = new BasisColorEditor(this, doc);
    connect(color_dialog, &BasisColorEditor::colorsChanged, this, &BasisEditor::onColorEditorModified);

    model = new QStandardItemModel();
    ui->tree->setModel(model);
    ui->tree->setAlternatingRowColors(true);
    ui->tree->setMouseTracking(true);

    model->setHorizontalHeaderLabels({"Name", "X", "Y", "Z", "Occ"});
    DoubleDelegate* pos_delg = new DoubleDelegate(-99999, 99999);
    DoubleDelegate* occ_delg = new DoubleDelegate(0, 1);
    BasisNameDelegate* name_delg = new BasisNameDelegate();
    ui->tree->setItemDelegateForColumn(0, name_delg);
    ui->tree->setItemDelegateForColumn(1, pos_delg);
    ui->tree->setItemDelegateForColumn(2, pos_delg);
    ui->tree->setItemDelegateForColumn(3, pos_delg);
    ui->tree->setItemDelegateForColumn(4, occ_delg);

    ui->tree->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tree->setSelectionMode(QAbstractItemView::SelectionMode::ExtendedSelection);

    ui->tree->header()->resizeSection(0, 110);
    ui->tree->header()->resizeSection(1, 60);
    ui->tree->header()->resizeSection(2, 60);
    ui->tree->header()->resizeSection(3, 60);
    ui->tree->header()->resizeSection(4, 60);

    connect(doc, &Document::basisPosAdded, this, &BasisEditor::onBasisPosAdded);
    connect(doc, &Document::basesCleared, this, &BasisEditor::onBasesCleared);
    connect(doc, &Document::basisPosRemovedId, this, &BasisEditor::onBasisPosRemoved);
    connect(doc, &Document::elementRemoved, this, &BasisEditor::onElementRemoved);
    connect(this->model, &QStandardItemModel::itemChanged, this, &BasisEditor::onItemChanged);
}


BasisEditor::~BasisEditor(){
    delete ui;
}

std::vector<QAction*> BasisEditor::toolbarActions() {
    std::vector<QAction*> retn;
    QAction* edit = new QAction(ui->modify_btn->icon(), ui->modify_btn->toolTip(), this);
    connect(edit, &QAction::triggered, ui->modify_btn, &QToolButton::clicked);
    retn.push_back(edit);

    QAction* newpos = new QAction(ui->newpos_btn->icon(), ui->newpos_btn->toolTip(), this);
    connect(newpos, &QAction::triggered, ui->newpos_btn, &QToolButton::clicked);
    retn.push_back(newpos);

    QAction* colors = new QAction(ui->colors_btn->icon(), ui->colors_btn->toolTip(), this);
    connect(colors, &QAction::triggered, ui->colors_btn, &QToolButton::clicked);
    retn.push_back(colors);

    return retn;
}

void BasisEditor::onBasisPosAdded(BasisPos* newpos){
    auto positem = BasisEditor::makeBasisItems(*newpos);
    auto elemitem = findElementItem(newpos->species());

    if (elemitem != nullptr){
        elemitem->appendRow(positem);

        // If the new basis position is the first of the element, expand the element item
        if (elemitem->rowCount() == 1){
            ui->tree->expand(model->indexFromItem(elemitem));
        }
    }
    // Add new element item if none previously existed
    else {
        auto newelem = BasisEditor::makeElementItem(newpos->species());
        model->invisibleRootItem()->appendRow(newelem);
        newelem->appendRow(positem);
        ui->tree->setExpanded(model->indexFromItem(newelem), true);
        auto row = newelem->row();
        ui->tree->setFirstColumnSpanned(row, QModelIndex(), true);
    }
}


void BasisEditor::onBasisPosRemoved(unsigned int id) {
    for (int i=0; i<model->rowCount(); ++i) {
        QModelIndex idx = model->index(i, 0, QModelIndex());
        QStandardItem* item = model->itemFromIndex(idx);
        for (int j=0; j<item->rowCount(); ++j) {
            QModelIndex idx2 = model->index(j, 0, idx);
            QStandardItem* item2 = model->itemFromIndex(idx2);
            if (item2->data(Qt::UserRole).toUInt() == id) {
                model->removeRow(idx2.row(), idx);
                return;
            }
        }
    }
}


void BasisEditor::onElementRemoved(chem::Element const* elem) {
    for (int i=0; i<model->rowCount(); ++i) {
        QModelIndex idx = model->index(i, 0, QModelIndex());
        QStandardItem* item = model->itemFromIndex(idx);
        QVariant type = item->data(Qt::UserRole);
        chem::Element const* curr_elem = qvariant_cast<chem::Element const*>(type);
        if (curr_elem == elem) {
            model->removeRow(idx.row(), QModelIndex());
            return;
        }
    }
}


QList<QStandardItem*> BasisEditor::makeBasisItems(BasisPos const& pos){
    QList<QStandardItem*> retn {};

    std::stringstream x_ss {};
    x_ss << std::fixed << std::setprecision(4) <<  pos.x();
    std::stringstream y_ss {};
    y_ss << std::fixed << std::setprecision(4) <<  pos.y();
    std::stringstream z_ss {};
    z_ss << std::fixed << std::setprecision(4) <<  pos.z();
    std::stringstream occ_ss {};
    occ_ss << std::fixed << std::setprecision(4) <<  pos.occupancy();
//    std::stringstream rad {};
//    rad << std::fixed << std::setprecision(2) <<  pos.radius();

    QStandardItem* item_lbl = new QStandardItem();
    item_lbl->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable);
    item_lbl->setData(pos.id(), Qt::UserRole);
    item_lbl->setData(QString::fromStdString(pos.label()), Qt::EditRole);

    QStandardItem* item_x = new QStandardItem();
    item_x->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable);
    item_x->setData(pos.id(), Qt::UserRole);
    item_x->setData(QString::fromStdString(x_ss.str()), Qt::EditRole);

    QStandardItem* item_y = new QStandardItem();
    item_y->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable);
    item_y->setData(pos.id(), Qt::UserRole);
    item_y->setData(QString::fromStdString(y_ss.str()), Qt::EditRole);

    QStandardItem* item_z = new QStandardItem();
    item_z->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable);
    item_z->setData(pos.id(), Qt::UserRole);
    item_z->setData(QString::fromStdString(z_ss.str()), Qt::EditRole);
//    retn << item_lbl << item_x << item_y << item_z;

    QStandardItem* item_occ = new QStandardItem();
    item_occ->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable);
    item_occ->setData(pos.id(), Qt::UserRole);
    item_occ->setData(QString::fromStdString(occ_ss.str()), Qt::EditRole);
    retn << item_lbl << item_x << item_y << item_z << item_occ;

    return retn;
}


QStandardItem* BasisEditor::makeElementItem(const chem::Element* elem){
    QStandardItem* item = new QStandardItem();
    item->setText(QString::fromStdString(elem->name));
    item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    QVariant v;
    v.setValue(elem);
    item->setData(v, Qt::UserRole);
    QFont font {};
    font.setBold(true);
    item->setData(font, Qt::FontRole);
    return item;
}


QStandardItem* BasisEditor::findElementItem(chem::Element const* elem){
    auto root = model->invisibleRootItem();
    for (int i=0; i<root->rowCount(); ++i){
        auto child = root->child(i, 0);
        QVariant v = child->data(Qt::UserRole);
        chem::Element const* item_elem = v.value<chem::Element const*>();
        if (item_elem == elem){
            return child;
        }
    }
    return nullptr;
}


void BasisEditor::onItemChanged(QStandardItem* item){
    emit doc->writableModified();
    auto id = item->data(Qt::UserRole).toUInt();
    BasisPos* pos = doc->findBasisPosById(id);
    auto col = item->column();

    switch (col) {
    case 0: {
        pos->setLabel(item->data(Qt::EditRole).toString().toStdString());
        break;
    }
    case 1: {
        pos->setX(item->data(Qt::EditRole).toDouble());
        emit emitReconstructView();
        break;
    }
    case 2: {
        pos->setY(item->data(Qt::EditRole).toDouble());
        emit emitReconstructView();
        break;
    }
    case 3: {
        pos->setZ(item->data(Qt::EditRole).toDouble());
        emit emitReconstructView();
        break;
    }
    case 4: {
        pos->setOccupancy(item->data(Qt::EditRole).toDouble());
        emit emitReconstructView();
        break;
    }
    }
}


std::vector<chem::Element const*> BasisEditor::elements(){
    std::vector<chem::Element const*> retn {};
    for (int i=0; i<model->rowCount(); ++i){
        auto child = model->item(i, 0);
        QVariant v = child->data(Qt::UserRole);
        chem::Element const* elem = v.value<chem::Element const*>();
        if (std::find(retn.begin(), retn.end(), elem) == retn.end()){
            retn.push_back(elem);
        }
    }
    return retn;
}


void BasisEditor::on_modify_btn_clicked(){
    auto newelem = ChooseElementDialog::getNewElements(this, elements());
    for (auto& i: newelem){
        auto newelem_item = BasisEditor::makeElementItem(i);
        model->invisibleRootItem()->appendRow(newelem_item);
        ui->tree->setFirstColumnSpanned(newelem_item->row(), QModelIndex(), true);
    }
}


void BasisEditor::on_clear_btn_clicked(){
    doc->clearBases();
}


void BasisEditor::onBasesCleared(){
    model->removeRows(0, model->rowCount());
}


void BasisEditor::on_newpos_btn_clicked(){
    if (elements().size() == 0){
        QMessageBox::warning(this, "No elements", "Cannot add basis positions with no elements.");
        return;
    }
    NewBasisPosDialog* diag = new NewBasisPosDialog(this, doc);
    diag->exec();
}

void BasisEditor::on_del_selected_btn_clicked() {
    doc->suppress_view_update = true;
    auto selected = ui->tree->selectionModel()->selectedIndexes();

    // Do nothing if nothing is selected
    if (selected.size() == 0){
        return;
    }

    for (auto& i: selected){
        // Since we only want to operate once per row, we will skip any item whos col is not 0
        if (i.column() != 0){
            continue;
        }

        // If the item is an element item
        if (i.parent() == QModelIndex()){
            auto elem = i.data(Qt::UserRole).value<chem::Element const*>();

            // This should never happen
            if (elem == nullptr){
                throw std::runtime_error("received nullptr when casting QVariant to Element");
            }
            doc->removeElement(elem);
        }
        // Otherwise the item is a basis position
        else {
            auto id = i.data(Qt::UserRole).toUInt();
            doc->removeBasisPosById(id);
        }
    }
    doc->suppress_view_update = false;
    emit emitReconstructView();
}

void BasisEditor::on_colors_btn_clicked() {
    color_dialog->show();
}


void BasisEditor::onColorEditorModified() {
    ui->tree->viewport()->repaint();
    emit emitReconstructView();
}
