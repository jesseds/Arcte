/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "elementcoloreditbutton.h"
#include <QColorDialog>
#include <QSizePolicy>
#include <QHBoxLayout>

ElementColorEditButton::ElementColorEditButton(QWidget* parent, unsigned int elem_num)
    : QWidget(parent), elem_num(elem_num) {
    elem = chem::table.findByNumber(elem_num);

    settings = new QSettings();
    btn = new QToolButton(this);
    QHBoxLayout* layout = new QHBoxLayout();
    setLayout(layout);
    layout->addWidget(btn);
    layout->addStretch();
    layout->setContentsMargins(0, 0, 0, 0);

    btn->setText("...");
    setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);

    connect(btn, &QToolButton::clicked, this, &ElementColorEditButton::onClicked);
}


void ElementColorEditButton::onClicked() {
    QColor curr_col {};
    curr_col.setRedF(elem->color()[0]);
    curr_col.setGreenF(elem->color()[1]);
    curr_col.setBlueF(elem->color()[2]);
    QColor newcol = QColorDialog::getColor(curr_col, this);
    if (! newcol.isValid()) {
        return;
    }
    if (newcol != curr_col) {
        std::string set_str = "elem_color_override/" + std::to_string(elem_num);
        settings->setValue(QString::fromStdString(set_str), newcol);
        emit colorsModified();
    }
}

