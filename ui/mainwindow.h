/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include <QMainWindow>
#include <scenewidget.h>
#include <document.h>
#include <QDir>
#include <QCloseEvent>
#include <QDockWidget>
#include <QObject>
#include <basiseditor.h>
#include <bondeditor.h>
#include <vieworientationdialog.h>
#include <calculatordialog.h>
#include <measuredistancedialog.h>
#include <measurebondangledialog.h>
#include <zoneaxispatterndialog.h>
#include <structurefactorsdialog.h>
#include <bundledstructdialog.h>
#include <xrddialog.h>
#include <vector>
#include <QSpinBox>
#include <greeter.h>
#include <framelesswidgetshelper.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void virtual closeEvent(QCloseEvent* event) override;
    Ui::MainWindow *ui;
    SceneWidget* viewport;
    void reconstructScene();
    void reconstructSceneAndRender();

    void hideEvent(QHideEvent* event) override;
    void showEvent(QShowEvent* event) override;

private:
    std::unique_ptr<Document> doc;
    QString last_dir {};
    BasisEditor* basisedit;
    BondEditor* bondedit;
    int last_radii_id;
    QString last_radii_txt;

    // Dialogs
    ViewOrientationDialog* uvw_win;
    CalculatorDialog* calc_win;
    MeasureDistanceDialog* dist_win;
    MeasureBondAngleDialog* bondangle_win;
    XrdDialog* xrd_win;
    ZoneAxisPatternDialog* zone_pattern_win;
    StructureFactorsDialog* structure_factors_win;
    BundledStructDialog* bundled_struct_win;

    QSpinBox* view_degrees;
    QSettings* settings;
    QByteArray window_state;
    Greeter* greeter;

    void closeAllDialogs();

    bool unsaved_changes {false};

    void createDocks();
    void setWindowTitleNewFile(QString const& file);
    void setWindowTitleNew();
    void defaultUi();
    QStringList recentFiles();
    void addRecentFiles(QString);
    void populateRecentFiles();
    QMenu* recent_files_menu;

    void showGreeter();
    void showViewport();

    void dragEnterEvent(QDragEnterEvent* e) override;
    void dropEvent(QDropEvent* e) override;

public slots:
    void loadCifFile(QFileInfo);

private slots:
    void on_actionOpen_triggered();
    void on_actionNew_triggered();
    void on_actionClose_triggered();
    void on_actionSave_triggered();
    void on_spg_edit_clicked();
    void on_actionProjection_toggled(bool arg1);
//    void on_atom_radii_select_currentIndexChanged(const QString &arg1);
    void on_atom_radii_select_currentTextChanged(const QString &arg1);
    void on_a_edit_editingFinished();
    void on_b_edit_editingFinished();
    void on_c_edit_editingFinished();
    void on_nx_edit_editingFinished();
    void on_ny_edit_editingFinished();
    void on_nz_edit_editingFinished();
    void on_alpha_edit_editingFinished();
    void on_beta_edit_editingFinished();
    void on_gamma_edit_editingFinished();
    void fillSpgInfo();
    void on_aa_edit_stateChanged(int arg1);
    void on_bkgcolor_edit_clicked();
    void on_radscale_edit_editingFinished();
    void on_wyc_btn_clicked();
    void on_mesh_style_currentTextChanged(QString const text);
    void on_actionResetCamera_triggered();
    void on_unit_cell_width_editingFinished();
    void on_unitcell_groupbox_toggled(bool checked);
    void on_bond_radii_edit_editingFinished();
    void on_bond_col_btn_clicked();
    void on_rendering_box_currentTextChanged(const QString &arg1);
    void on_roughness_valueChanged(double arg1);
    void on_actionExport_image_triggered();
    void on_actionExport_model_triggered();
    void on_actionAbout_triggered();
    void on_actionSaveAs_triggered();

    void on_actionView_uvw_triggered();
    void on_actionRollUp_triggered();
    void on_actionRollDown_triggered();
    void on_actionYawUp_triggered();
    void on_actionYawDown_triggered();
    void on_actionPitchUp_triggered();
    void on_actionPitchDown_triggered();

    void onWritableModified();
    void onDocRadiiTypeChanged(AtomRadius);
    void on_actionCalculator_triggered();
    void on_bond_style_edit_currentIndexChanged(int index);
    void on_actionGo_to_webpage_triggered();
    void on_actionCheck_for_updates_triggered();
    void on_atoms_groupbox_toggled(bool checked);
    void on_bonds_groupbox_toggled(bool checked);
    void on_actionMeasure_distance_triggered();
    void on_actionMeasure_bond_angle_triggered();
    void on_actionXrd_triggered();
    void on_actionZone_axis_diffraction_pattern_triggered();
    void on_actionStructure_factors_triggered();
    void on_actionLoad_bundled_triggered();
};

