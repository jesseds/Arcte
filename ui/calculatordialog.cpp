/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "calculatordialog.h"
#include "ui_calculatordialog.h"
#include <geometry.h>
#include <ciso646>
#include <QKeyEvent>
#include <QTextStream>
#include <QDebug>

using std::isnan;

CalculatorDialog::CalculatorDialog(QWidget *parent, Document const* doc) :
    QDialog(parent),
    ui(new Ui::CalculatorDialog) {
    ui->setupUi(this);

    this->doc = doc;
    ui->copy_from_doc_btn->setDefault(false);
    ui->copy_from_doc_btn->setAutoDefault(false);

    on_angle_spacing_lbl_clicked();
    on_wl_spacing_lbl_clicked();

    ui->angle_invalid->setVisible(false);
    ui->wl_invalid->setVisible(false);
    ui->spacing_invalid->setVisible(false);

    connect(ui->a_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->b_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->c_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->alpha_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->beta_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->gamma_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);

    connect(ui->u1_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->v1_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->w1_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->u2_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->v2_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->w2_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);

    connect(ui->h1_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->k1_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->l1_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->h2_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->k2_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->l2_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);

    connect(ui->angle_n_edit, qOverload<int>(&QSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->angle_spacing_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->angle_freq_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->angle_wl_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);

    connect(ui->wl_n_edit, qOverload<int>(&QSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->wl_spacing_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->wl_freq_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->wl_angle_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);

    connect(ui->spacing_angle_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->spacing_n_edit, qOverload<int>(&QSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->spacing_wl_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);

    connect(ui->electron_energy_edit, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);

    connect(ui->weiss_u, qOverload<int>(&QSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->weiss_v, qOverload<int>(&QSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->weiss_w, qOverload<int>(&QSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->weiss_h, qOverload<int>(&QSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->weiss_k, qOverload<int>(&QSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->weiss_l, qOverload<int>(&QSpinBox::valueChanged), this, &CalculatorDialog::compute);

    connect(ui->zone_h1, qOverload<int>(&QSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->zone_k1, qOverload<int>(&QSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->zone_l1, qOverload<int>(&QSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->zone_h2, qOverload<int>(&QSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->zone_k2, qOverload<int>(&QSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->zone_l2, qOverload<int>(&QSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->zone_h3, qOverload<int>(&QSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->zone_k3, qOverload<int>(&QSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->zone_l3, qOverload<int>(&QSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->zone_3axis_check, qOverload<bool>(&QCheckBox::toggled), this, &CalculatorDialog::compute);
    connect(ui->zone_3axis_check, &QCheckBox::toggled, ui->zone_h3, &QSpinBox::setEnabled);
    connect(ui->zone_3axis_check, &QCheckBox::toggled, ui->zone_k3, &QSpinBox::setEnabled);
    connect(ui->zone_3axis_check, &QCheckBox::toggled, ui->zone_l3, &QSpinBox::setEnabled);
    connect(ui->tilt_a1, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->tilt_b1, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->tilt_a2, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);
    connect(ui->tilt_b2, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &CalculatorDialog::compute);

    // Resize to smallest possible size
    resize(1, 1);
    }

CalculatorDialog::~CalculatorDialog() {
    delete ui;
}

void CalculatorDialog::showWithDocumentParams(Document const* doc) {
    this->doc = doc;
    reset();
    on_copy_from_doc_btn_clicked();
    show();
}

void CalculatorDialog::compute() {
    // Recalculate and display all fields
    auto a = ui->a_edit->value();
    auto b = ui->b_edit->value();
    auto c = ui->c_edit->value();
    auto alpha = ui->alpha_edit->value();
    auto beta = ui->beta_edit->value();
    auto gamma = ui->gamma_edit->value();

    bool valid_cell = geom::validCellAngles(alpha, beta, gamma);
    if (not valid_cell) {
        ui->invalid_cell->setVisible(true);
        return;;
    }
    else {
        ui->invalid_cell->setVisible(false);
    }

    auto rparams = geom::reciprocalParams(a, b, c, alpha, beta, gamma);
    double ar = rparams[0];
    double br = rparams[1];
    double cr = rparams[2];
    double alphar = rparams[3];
    double betar = rparams[4];
    double gammar = rparams[5];

    // Reciprocal lattice parameters
    ui->ar_edit->setValue(ar);
    ui->br_edit->setValue(br);
    ui->cr_edit->setValue(cr);
    ui->alphar_edit->setValue(alphar);
    ui->betar_edit->setValue(betar);
    ui->gammar_edit->setValue(gammar);

    // Volume fields
    ui->real_vol_edit->setValue(geom::volume(a, b, c, alpha, beta, gamma));
    ui->reciprocal_vol_edit->setValue(geom::volume(ar, br, cr, alphar, betar, gammar));

    // De/orthogonalization matrices
    auto deortho = geom::deorthogonalization(a, b, c, alpha, beta, gamma);
    auto deortho_r = geom::deorthogonalization(ar, br, cr, alphar, betar, gammar);
    auto ortho = deortho.inverse();
    ui->ortho00->setValue(ortho(0, 0));
    ui->ortho01->setValue(ortho(0, 1));
    ui->ortho02->setValue(ortho(0, 2));
    ui->ortho10->setValue(ortho(1, 0));
    ui->ortho11->setValue(ortho(1, 1));
    ui->ortho12->setValue(ortho(1, 2));
    ui->ortho20->setValue(ortho(2, 0));
    ui->ortho21->setValue(ortho(2, 1));
    ui->ortho22->setValue(ortho(2, 2));

//    ui->orthor00->setValue(deortho(0, 0));
//    ui->orthor01->setValue(deortho(0, 1));
//    ui->orthor02->setValue(deortho(0, 2));
//    ui->orthor10->setValue(deortho(1, 0));
//    ui->orthor11->setValue(deortho(1, 1));
//    ui->orthor12->setValue(deortho(1, 2));
//    ui->orthor20->setValue(deortho(2, 0));
//    ui->orthor21->setValue(deortho(2, 1));
//    ui->orthor22->setValue(deortho(2, 2));

    // Metric tensors
    auto mtensor = geom::metricTensor(a, b, c, alpha, beta, gamma);
    auto mtensor_r = mtensor.inverse();
    ui->gam00->setValue(mtensor(0, 0));
    ui->gam01->setValue(mtensor(0, 1));
    ui->gam02->setValue(mtensor(0, 2));
    ui->gam10->setValue(mtensor(1, 0));
    ui->gam11->setValue(mtensor(1, 1));
    ui->gam12->setValue(mtensor(1, 2));
    ui->gam20->setValue(mtensor(2, 0));
    ui->gam21->setValue(mtensor(2, 1));
    ui->gam22->setValue(mtensor(2, 2));

    ui->gamr00->setValue(mtensor_r(0, 0));
    ui->gamr01->setValue(mtensor_r(0, 1));
    ui->gamr02->setValue(mtensor_r(0, 2));
    ui->gamr10->setValue(mtensor_r(1, 0));
    ui->gamr11->setValue(mtensor_r(1, 1));
    ui->gamr12->setValue(mtensor_r(1, 2));
    ui->gamr20->setValue(mtensor_r(2, 0));
    ui->gamr21->setValue(mtensor_r(2, 1));
    ui->gamr22->setValue(mtensor_r(2, 2));

    // Crystallographic vectors
    u1 = ui->u1_edit->value();
    u2 = ui->u2_edit->value();
    v1 = ui->v1_edit->value();
    v2 = ui->v2_edit->value();
    w1 = ui->w1_edit->value();
    w2 = ui->w2_edit->value();

    double uvw1_dist = geom::uvwDist(mtensor, u1, v1, w1);
    ui->uvsw1_dist_edit->setValue(uvw1_dist);

    double uvw2_dist = geom::uvwDist(mtensor, u2, v2, w2);
    ui->uvsw2_dist_edit->setValue(uvw2_dist);

    if (ui->uvsw1_dist_edit->value() !=0 and ui->uvsw2_dist_edit->value() != 0) {
        double uvw_angle = geom::vecAngle(deortho, u1, v1, w1, u2, v2, w2);
        ui->uvsw_angle_edit->setValue(uvw_angle);
    }
    else {
        ui->uvsw_angle_edit->setValue(0);
    }

    Eigen::Vector3d uvw1 {u1, v1, w1};
    Eigen::Vector3d uvw2 {u2, v2, w2};
    double uvw_dot = uvw1.dot(uvw2);
    Eigen::Vector3d uvw_cross = uvw1.cross(uvw2);
    ui->uvsw_dot->setValue(uvw_dot);

    std::stringstream uvw_cross_str {};
    uvw_cross_str << "(" << std::setprecision(4) << uvw_cross(0) << ", ";
    uvw_cross_str << uvw_cross(1) << ", ";
    uvw_cross_str << uvw_cross(2) << ")";
    ui->uvsw_cross->setText(QString::fromStdString(uvw_cross_str.str()));

    // Reciprocal lattice vectors
    h1 = ui->h1_edit->value();
    h2 = ui->h2_edit->value();
    k1 = ui->k1_edit->value();
    k2 = ui->k2_edit->value();
    l1 = ui->l1_edit->value();
    l2 = ui->l2_edit->value();

        double hkl1_dist = geom::hklDist(mtensor_r, h1, k1, l1);
        ui->hkil1_dist_edit->setValue(hkl1_dist);
        if (hkl1_dist != 0) {
            ui->hkil1_spacing_edit->setValue(1/hkl1_dist);
        }
        else {
            ui->hkil1_spacing_edit->setValue(0);
        }

        double hkl2_dist = geom::hklDist(mtensor_r, h2, k2, l2);
        ui->hkil2_dist_edit->setValue(hkl2_dist);
        if (hkl2_dist != 0) {
            ui->hkil2_spacing_edit->setValue(1/hkl2_dist);
        }
        else {
            ui->hkil2_spacing_edit->setValue(0);
        }

    if (ui->hkil1_dist_edit->value() !=0 and ui->hkil2_dist_edit->value() != 0) {
        double hkl_angle = geom::vecAngle(deortho_r, h1, k1, l1, h2, k2, l2);
        ui->hkil_angle_edit->setValue(hkl_angle);
    }
    else {
        ui->hkil_angle_edit->setValue(0);
    }

    Eigen::Vector3d hkl1 {h1, k1, l1};
    Eigen::Vector3d hkl2 {h2, k2, l2};
    double hkl_dot = hkl1.dot(hkl2);
    Eigen::Vector3d hkl_cross = hkl1.cross(hkl2);
    ui->hkil_dot->setValue(hkl_dot);

    std::stringstream hkl_cross_str {};
    hkl_cross_str << "(" << std::setprecision(4) << hkl_cross(0) << ", ";
    hkl_cross_str << hkl_cross(1) << ", ";
    hkl_cross_str << hkl_cross(2) << ")";
    ui->hkil_cross->setText(QString::fromStdString(hkl_cross_str.str()));

    // Diffraction bragg angle
    dhkl = ui->angle_spacing_edit->value() * 1e-10;
    ghkl = ui->angle_freq_edit->value() * 1e10;
    wl = ui->angle_wl_edit->value() * 1e-12;
    n = ui->angle_n_edit->value();

    if (ui->angle_spacing_lbl->isChecked()) {
        double val = geom::braggAngle(wl, dhkl, n);
        if (isnan(val)) {
            ui->angle_angle_edit->setValue(0);
            ui->angle_invalid->setVisible(true);
        }
        else {
            ui->angle_angle_edit->setValue(val);
            ui->angle_invalid->setVisible(false);
        }
    }
    else {
        double val = geom::braggAngle(wl, 1/ghkl, n);
        if (isnan(val)) {
            ui->angle_angle_edit->setValue(0);
            ui->angle_invalid->setVisible(true);
        }
        else {
            ui->angle_angle_edit->setValue(val);
            ui->angle_invalid->setVisible(false);
        }
    }

    // Diffraction bragg wavelength
    dhkl = ui->wl_spacing_edit->value() * 1e-10;
    ghkl = ui->wl_freq_edit->value() * 1e10;
    angle = ui->wl_angle_edit->value();
    n = ui->wl_n_edit->value();

    if (ui->wl_spacing_lbl->isChecked()) {
        double val = geom::braggWl(angle, dhkl, n) * 1e12;
        if (isnan(val)) {
            ui->wl_invalid->setVisible(true);
            ui->wl_wl_edit->setValue(0);
        }
        else {
            ui->wl_wl_edit->setValue(val);
            ui->wl_invalid->setVisible(false);
        }
    }
    else {
        double val = geom::braggWl(angle, 1/ghkl, n) * 1e12;
        if (isnan(val)) {
            ui->wl_invalid->setVisible(true);
            ui->wl_wl_edit->setValue(0);
        }
        else {
            ui->wl_invalid->setVisible(false);
            ui->wl_wl_edit->setValue(val);
        }
    }

    // Diffraction bragg spacing/frequency
    angle = ui->spacing_angle_edit->value();
    n = ui->spacing_n_edit->value();
    wl = ui->spacing_wl_edit->value() * 1e-12;
    dhkl = geom::braggDHkl(wl, angle, n) * 1e10;
    ghkl = 1/dhkl;

    if (isnan(dhkl) or isnan(ghkl)) {
        ui->spacing_spacing_edit->setValue(0);
        ui->spacing_freq_edit->setValue(0);
        ui->spacing_invalid->setVisible(true);
    }
    else {
        ui->spacing_invalid->setVisible(false);
        ui->spacing_spacing_edit->setValue(dhkl);
        ui->spacing_freq_edit->setValue(ghkl);
    }

    // Relativistic electron
    volts = ui->electron_energy_edit->value();
    wl = geom::relElectronWl(volts) * 1e2; // Angstrom to pm for display
    mass = geom::relElectronMass(volts);
    std::stringstream mass_str {};
    mass_str << std::scientific;
    mass_str << mass << " kg";
    gamma = geom::relElectronGamma(volts);

    ui->electron_wl_edit->setValue(wl);
    ui->electron_mass_edit->setText(QString::fromStdString(mass_str.str()));
    ui->electron_gamma_edit->setValue(gamma);

    // Weiss zone law
    std::array<int, 3> weiss_uvw = {ui->weiss_u->value(), ui->weiss_v->value(), ui->weiss_w->value()};
    std::array<int, 3> weiss_hkl = {ui->weiss_h->value(), ui->weiss_k->value(), ui->weiss_l->value()};
    int weiss_val = weiss_uvw[0]*weiss_hkl[0] + weiss_uvw[1]*weiss_hkl[1] + weiss_uvw[2]*weiss_hkl[2] ;
    ui->weiss_result->setText("hu + kv + lw = " + QString::fromStdString(std::to_string(weiss_val)));

    // Zone axis calculation
    Vector3i zone_vec1 {};
    zone_vec1 << ui->zone_h1->value(), ui->zone_k1->value(), ui->zone_l1->value();

    Vector3i zone_vec2 {};
    zone_vec2 << ui->zone_h2->value(), ui->zone_k2->value(), ui->zone_l2->value();

    Vector3i zone_vec3 {};
    zone_vec3 << ui->zone_h3->value(), ui->zone_k3->value(), ui->zone_l3->value();

    auto zone_axis = geom::zoneAxis2(zone_vec1, zone_vec2);
    bool all_3_on_zone = geom::zoneAxis3(zone_vec1, zone_vec2, zone_vec3);

    std::stringstream zone_str {};
    zone_str << "[" << zone_axis(0) << ", " << zone_axis(1) << ", " << zone_axis(2) << "]";

    std::string zone_display_str = "";

    if ((ui->zone_3axis_check->isChecked() and not all_3_on_zone) or zone_axis.isZero()) {
        zone_display_str = "No zone axis";
    }
    else {
        zone_display_str = zone_str.str();
    }

    ui->zone_result_lbl->setText(QString::fromStdString(zone_display_str));

    // TEM stage tilts
    double a1 = geom::deg2rad(ui->tilt_a1->value());
    double b1 = geom::deg2rad(ui->tilt_b1->value());
    double a2 = geom::deg2rad(ui->tilt_a2->value());
    double b2 = geom::deg2rad(ui->tilt_b2->value());

    Vector3d tilt_vec1 {};
    tilt_vec1 << std::cos(b1) * std::sin(a1), cos(a1) * cos(b1), std::sin(b1);

    Vector3d tilt_vec2 {};
    tilt_vec2 << std::cos(b2) * std::sin(a2), cos(a2) * cos(b2), std::sin(b2);

    double tilt = ( (tilt_vec1.transpose() * tilt_vec2) / tilt_vec1.norm() * tilt_vec2.norm() ).array().acos()(0);
    tilt = geom::rad2deg(tilt);
    ui->tilt_result->setText("Angle spanning tilt: " + QString::number(tilt, 'f', 4));

}

void CalculatorDialog::reset() {
    ui->a_edit->setValue(0);
    ui->b_edit->setValue(0);
    ui->c_edit->setValue(0);
    ui->alpha_edit->setValue(0);
    ui->beta_edit->setValue(0);
    ui->gamma_edit->setValue(0);

    ui->u1_edit->setValue(0);
    ui->v1_edit->setValue(0);
    ui->w1_edit->setValue(0);
    ui->u2_edit->setValue(0);
    ui->v2_edit->setValue(0);
    ui->w2_edit->setValue(0);

    ui->h1_edit->setValue(0);
    ui->k1_edit->setValue(0);
    ui->l1_edit->setValue(0);
    ui->h2_edit->setValue(0);
    ui->k2_edit->setValue(0);
    ui->l2_edit->setValue(0);

    ui->angle_n_edit->setValue(1);
    ui->angle_wl_edit->setValue(154.6);
    ui->angle_spacing_edit->setValue(1);
    ui->angle_freq_edit->setValue(1);

    ui->wl_n_edit->setValue(1);
    ui->wl_angle_edit->setValue(50.6241);
    ui->wl_spacing_edit->setValue(1);
    ui->wl_freq_edit->setValue(1);

    ui->spacing_n_edit->setValue(1);
    ui->spacing_angle_edit->setValue(50.6241);
    ui->spacing_wl_edit->setValue(154.6);

}

void CalculatorDialog::on_copy_from_doc_btn_clicked() {
    ui->a_edit->setValue(doc->a());
    ui->b_edit->setValue(doc->b());
    ui->c_edit->setValue(doc->c());
    ui->alpha_edit->setValue(doc->alpha());
    ui->beta_edit->setValue(doc->beta());
    ui->gamma_edit->setValue(doc->gamma());
    compute();
}

void CalculatorDialog::on_angle_spacing_lbl_clicked() {
    ui->angle_freq_lbl->setChecked(false);
    ui->angle_freq_edit->setEnabled(false);
    ui->angle_spacing_edit->setEnabled(true);
    compute();
}

void CalculatorDialog::on_angle_freq_lbl_clicked() {
    ui->angle_spacing_lbl->setChecked(false);
    ui->angle_freq_edit->setEnabled(true);
    ui->angle_spacing_edit->setEnabled(false);
    compute();
}

void CalculatorDialog::on_wl_spacing_lbl_clicked() {
    ui->wl_freq_lbl->setChecked(false);
    ui->wl_freq_edit->setEnabled(false);
    ui->wl_spacing_edit->setEnabled(true);
    compute();
}

void CalculatorDialog::on_wl_freq_lbl_clicked() {
    ui->wl_spacing_lbl->setChecked(false);
    ui->wl_freq_edit->setEnabled(true);
    ui->wl_spacing_edit->setEnabled(false);
    compute();
}

