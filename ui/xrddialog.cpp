/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "xrddialog.h"
#include "ui_xrddialog.h"
#include <geometry.h>
#include <QFileDialog>
#include <QClipboard>
#include <qtabulate.h>

XrdDialog::XrdDialog(QWidget *parent, Document const* doc) :
    QMainWindow(parent),
    ui(new Ui::XrdDialog),
    doc(doc) {
    ui->setupUi(this);

    // Keep window on top like a dialog
    setWindowFlag(Qt::Dialog, true);

    ui->dock->setTitleBarWidget(new QWidget());

    // Simulation
    xrd_sims = std::make_unique<xrd::MultiSimulation>(doc);
    auto sim1 = xrd_sims->addSim(1.5406, 1);
    auto sim2 = xrd_sims->addSim(1.54443, 0);
    auto sim3 = xrd_sims->addSim(1.39225, 0);

    // Plotter tab
    chart = new XrdPlotter(this, xrd_sims.get());
    auto plot_layout  = new QVBoxLayout();
    plot_layout->setContentsMargins(0, 0, 0, 0);
    ui->tab_plot->setLayout(plot_layout);
    plot_layout->addWidget(chart);

    // Setup table tab
    tabulate = new QTabulate(this, xrd_sims->reflTable());
    auto table_layout = new QVBoxLayout();
    ui->tab_table->setLayout(table_layout);
    table_layout->addWidget(tabulate);
    table_layout->setContentsMargins(0, 0, 0, 0);

    // Toolbar
    addToolBar(chart->toolbar);
    addToolBar(tabulate->toolbar);

    // Initialize form values
    ui->show_hkls_edit->setChecked(chart->show_labels);
    ui->show_d_edit->setChecked(chart->show_d_spacing);
    ui->show_2theta_edit->setChecked(chart->show_2theta);
    ui->threshold_edit->setValue(chart->int_threshold);

    ui->wl1_edit->setValue(sim1->wavelength());
    ui->wl2_edit->setValue(sim2->wavelength());
    ui->wl3_edit->setValue(sim3->wavelength());
    ui->wl1_int_edit->setValue(sim1->scale());
    ui->wl2_int_edit->setValue(sim2->scale());
    ui->wl3_int_edit->setValue(sim3->scale());
    ui->range_min_edit->setValue(xrd_sims->rangeMin());
    ui->range_max_edit->setValue(xrd_sims->rangeMax());

//    connect(ui->calculate_btn, &QPushButton::clicked, xrd_sims.get(), &xrd::MultiSimulation::calculate);
    connect(ui->wl1_edit, QOverload<double>::of(&QDoubleSpinBox::valueChanged), sim1, &xrd::SingleSimulation::setWavelength);
    connect(ui->wl2_edit, QOverload<double>::of(&QDoubleSpinBox::valueChanged), sim2, &xrd::SingleSimulation::setWavelength);
    connect(ui->wl3_edit, QOverload<double>::of(&QDoubleSpinBox::valueChanged), sim3, &xrd::SingleSimulation::setWavelength);
    connect(ui->wl2_int_edit, QOverload<double>::of(&QDoubleSpinBox::valueChanged), sim2, &xrd::SingleSimulation::setScale);
    connect(ui->wl3_int_edit, QOverload<double>::of(&QDoubleSpinBox::valueChanged), sim3, &xrd::SingleSimulation::setScale);
    connect(ui->range_min_edit, QOverload<double>::of(&QDoubleSpinBox::valueChanged), xrd_sims.get(), &xrd::MultiSimulation::setRangeMin);
    connect(ui->range_max_edit, QOverload<double>::of(&QDoubleSpinBox::valueChanged), xrd_sims.get(), &xrd::MultiSimulation::setRangeMax);

    connect(ui->show_hkls_edit, &QCheckBox::toggled, chart, &XrdPlotter::onShowLabelsChanged);
    connect(ui->show_d_edit, &QCheckBox::toggled, chart, &XrdPlotter::onShowDSpacingChanged);
    connect(ui->show_2theta_edit, &QCheckBox::toggled, chart, &XrdPlotter::onShow2ThetaChanged);
    connect(ui->threshold_edit, QOverload<double>::of(&QDoubleSliderEdit::valueChanged), chart, &XrdPlotter::onIntThresholdChanged);

//    connect(ui->actionSave_image, &QAction::triggered, this, &XrdDialog::onSaveImageTriggered);
//    connect(ui->actionCopy_image, &QAction::triggered, this, &XrdDialog::onCopyImageTriggered);
}

void XrdDialog::show() {
    xrd_sims->calculate();
    QMainWindow::show();
}

//void XrdDialog::onSaveImageTriggered() {
//    QUrl url = QFileDialog::getSaveFileUrl(this, "Select save path", QDir::home().absolutePath(), "Portable Network Graphics (*.png)");
//    QPixmap pix = chart->chartAsPixmap();
//    pix.save(url.path());
//}

//void XrdDialog::onCopyImageTriggered() {
//    QPixmap pix = chart->chartAsPixmap();
//    auto clipboard = QApplication::clipboard();
//    clipboard->setImage(pix.toImage());
//}

XrdDialog::~XrdDialog() {
    delete ui;
}

//void XrdDialog::populateTable() {
//    ui->table_widget->clear();

//    auto count = 0;
//    for (auto const& sim: xrd_sims->allSims()) {
//        if (sim->scale() < 1e-5) {
//            ++count;
//            continue;
//        }
//        for (auto const& [theta, peak]: sim->condensedPeaks()) {
//            QTreeWidgetItem* row = new QTreeWidgetItem();
//            row->setData(0, Qt::DisplayRole, count+1);
//            row->setData(1, Qt::DisplayRole, peak.h);
//            row->setData(2, Qt::DisplayRole, peak.k);
//            row->setData(3, Qt::DisplayRole, peak.l);
//            row->setData(4, Qt::DisplayRole, peak.I);
//            row->setData(5, Qt::DisplayRole, peak.theta);
//            row->setData(6, Qt::DisplayRole, peak.theta*2);
//            row->setData(7, Qt::DisplayRole, 1/peak.g);
//            row->setData(8, Qt::DisplayRole, peak.g);

//            std::stringstream real {std::to_string(peak.Fhkl_real)};
//            real << std::setprecision(7);
//            row->setData(9, Qt::DisplayRole, QString::fromStdString(real.str()));

//            double imag_num = peak.Fhkl_imag;
//            if (abs(imag_num) > 1e-6) {
//                std::stringstream imag {std::to_string(imag_num)};
//                imag << std::setprecision(6);
//                row->setData(10, Qt::DisplayRole, QString::fromStdString(imag.str()));
//            }
//            else {
//                row->setData(10, Qt::DisplayRole, 0);

//            }
//            row->setData(11, Qt::DisplayRole, peak.m);
//            ui->table_widget->addTopLevelItem(row);
//        }
//        ui->table_widget->sortItems(7, Qt::SortOrder::DescendingOrder);
//        ++count;
//    }
//}

void XrdDialog::on_calculate_btn_clicked() {
    chart->clear();
    if (ui->range_min_edit->value() < ui->range_max_edit->value()) {
        xrd_sims->calculate();
        chart->setVisible(true);
    }
    else {
        chart->setVisible(false);
    }
//    populateTable();
}

