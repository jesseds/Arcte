/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <elements.h>
#include <iostream>
#include <memory>
#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <QFile>
#include <string>
#include <stdexcept>
#include <ciso646>
#include <cctype>
#include <QSettings>
#include <QColor>
#include <QUrl>
#include <QResource>
#include <QTextStream>


chem::Table::Table() : QObject () {
    load_data();
}

void chem::Table::load_data(){
    /*
     * Load from a csv of elemental information
     */

    QResource elem_rsc {":/data/elementaldata.csv"};

    auto qfile = new QFile(elem_rsc.absoluteFilePath());
    assert(qfile->exists());
    qfile->open(QIODevice::ReadOnly | QIODevice::Text);

    // First line is header
    qfile->readLine(0);
    QTextStream text (qfile);

    while (not text.atEnd()) {
        std::stringstream linestream {text.readLine().toStdString()};

        std::string number;
        std::string sym;
        std::string name;
        std::string atomic;
        std::string van_der_wall;
        std::string covalent_multiple;
        std::string metallic;
        std::string red;
        std::string green;
        std::string blue;
        std::string elneg;
        std::string kind;

        std::getline(linestream, number, ',');
        std::getline(linestream, sym, ',');
        std::getline(linestream, name, ',');
        std::getline(linestream, atomic, ',');
        std::getline(linestream, van_der_wall, ',');
        std::getline(linestream, covalent_multiple, ',');
        std::getline(linestream, metallic, ',');
        std::getline(linestream, red, ',');
        std::getline(linestream, green, ',');
        std::getline(linestream, blue, ',');
        std::getline(linestream, elneg, ',');
        std::getline(linestream, kind, ',');

        Element elem {};
        elem.name = name;
        elem.number = stoi(number);
        elem.symbol = sym;
        elem.setRadiiAtomic(stod(atomic));
        elem.setRadiiVanDerWall(stod(van_der_wall));
        elem.setRadiiCovalent(stod(covalent_multiple));
        elem.setRadiiMetallic(stod(metallic));
        std::array<double, 3> color {stod(red), stod(green), stod(blue)};
        elem.setColor(color);
        elem.setElectronegativity(stod(elneg));

        if (kind == "") {
            elem.setElementType(chem::ElementType::Unknown);
        }
        else if (kind == "Nonmetal") {
            elem.setElementType(chem::ElementType::NonMetal);
        }
        else if (kind == "Noble gas") {
            elem.setElementType(chem::ElementType::NobleGas);
        }
        else if (kind == "Alkali metal") {
            elem.setElementType(chem::ElementType::AlkaliMetal);
        }
        else if (kind == "Alkali earth metal") {
            elem.setElementType(chem::ElementType::AlkaliEarthMetal);
        }
        else if (kind == "Transition metal") {
            elem.setElementType(chem::ElementType::TransitionMetal);
        }
        else if (kind == "Post-transition metal") {
            elem.setElementType(chem::ElementType::PostTransitionMetal);
        }
        else if (kind == "Lanthanide") {
            elem.setElementType(chem::ElementType::Lanthanide);
        }
        else if (kind == "Actinide") {
            elem.setElementType(chem::ElementType::Actinide);
        }
        else if (kind == "Metalloid") {
            elem.setElementType(chem::ElementType::Metalloid);
        }
        else {
            throw std::runtime_error("chem::Table::load_data(): Could not interpret element type string");
        }

        this->addElement(std::move(elem));
    }
    qfile->close();
}

void chem::Element::setColor(std::array<double, 3> const& val){
    if (std::all_of(val.begin(), val.end(), [](double x){return x >= 0 and x<= 1;})){
        _color = val;
    }
    else {
        throw std::invalid_argument("Color must be an array of doubles between 0 and 1");
    }
}

std::array<double, 3> chem::Element::color() const {
    QSettings* settings = new QSettings();
    std::string set_str = "elem_color_override/" + std::to_string(number);
    if (settings->contains(QString::fromStdString(set_str))) {
        // Overriden color has been defined by user
        QColor col = settings->value(QString::fromStdString(set_str)).value<QColor>();
        if (! col.isValid()) {
            throw std::runtime_error("chem::Element: invalid user stored color");
        }

        return std::array<double, 3> {col.redF(), col.greenF(), col.blueF()};
    }
    else {
        return _color;
    }
}

bool chem::Element::isMetallic() const {
    switch (_type) {
        case chem::ElementType::AlkaliEarthMetal:
            return true;
        case chem::ElementType::AlkaliMetal:
            return true;
        case chem::ElementType::TransitionMetal:
            return true;
        case chem::ElementType::PostTransitionMetal:
            return true;
        case chem::ElementType::Lanthanide:
            return true;
        case chem::ElementType::Actinide:
            return true;
        default:
            return false;
    }
}

void chem::Table::addElement(Element const& newelem){
    elements.push_back(std::make_shared<Element const>(newelem));
}

chem::Element const* chem::Table::findBySym(std::string const& sym) const {
    for (auto const& i: elements){
        if (i->symbol == sym){
            return &*i;
        }
    }
    throw std::invalid_argument("element symbol not found");
}

chem::Element const* chem::Table::findByName(std::string const& name) const {
    for (auto const& i: elements){
        if (i->name == name){
            return &*i;
        }
    }
    throw std::invalid_argument("element name not found");
}

chem::Element const* chem::Table::findByNumber(unsigned int number) const {
    for (auto const& i: elements){
        if (i->number == number){
            return &*i;
        }
    }
    throw std::invalid_argument("element name not found");
}

vtkSmartPointer<vtkLookupTable> chem::Table::lookupTable() const {
    vtkNew<vtkLookupTable> retn;
//    retn->SetTableRange(0, 118);
    for (auto const& elem: this->elements) {
        retn->SetTableValue(elem->number,
                            elem->color()[0],
                            elem->color()[1],
                            elem->color()[2]
                            );
    }
    retn->Build();
    return retn;
}

std::vector<std::string> chem::split_element_symbols(std::string instring){
    // Remove non alphabetical characters
    instring.erase(std::remove_if(instring.begin(), instring.end(), [](char i){return ! std::isalpha(i);}), instring.end());

    std::vector<std::string> split_symbols {};
    for (auto i: instring){
            // Uppercase letter indicates a new symbol
            if (std::isupper(i)){
                std::string new_sym {i};
                split_symbols.push_back(new_sym);
            }
            // Lowercase indicates a continuation of last symbol
            if (std::islower(i)){
                split_symbols.back() += i;
            }
    }
    return split_symbols;
}

chem::Element const* chem::str2symbol(std::string const& elem_str){
    /*
     * Given a string, search for an occurence of an elemental symbol (i.e. Si) and return the corresponding enum.
     * If the string contains multiple, or no matches then an exception will be thrown
     */
    std::vector<std::string> symbols = split_element_symbols(elem_str);

    if (symbols.size() == 1){
        // Single element found
        auto matches = std::find_if(chem::table.begin(), chem::table.end(), [&](auto& x){return x->symbol == symbols[0];});
        return &*matches[0];
    }
    if (symbols.size() > 1){
        throw std::runtime_error("chem::str2symbol(): Found more than 1 distinct elemental symbols");
    }
    else {
        throw std::runtime_error("chem::str2symbol(): No elemental symbols found in string");
    }
}

chem::Table const chem::table = chem::Table{};
