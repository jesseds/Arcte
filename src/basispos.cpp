/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <basispos.h>
#include <iostream>
#include <ciso646>


// id should never be 0, 0 is used for when actor_ptr has not set a kind_id
unsigned int BasisPos::next_id = 1;


BasisPos::BasisPos() :
    _species(chem::table.findBySym("null")) {
    // Already made sure _species was set in initializer list, we do it again to ensure its signal was emitted
    setSpecies(chem::table.findBySym("null"));
    setId();
}

BasisPos::BasisPos(const double& x, const double& y, const double& z, chem::Element const* species,
                   const std::string& label, std::string const& type_name, double occupancy) {
    setX(x);
    setY(y);
    setZ(z);
    setSpecies(species);
    setLabel(label);
    setTypeName(type_name);
    setId();
    setOccupancy(occupancy);
}

void BasisPos::setId() {
    // Set a unique ID so that we can map changes to/from the BasisEditor model/delegate
    _id = BasisPos::next_id;
    BasisPos::next_id++;
}


void BasisPos::setX(double val) {
    _x = val;
}

void BasisPos::setY(double val) {
    _y = val;
}

void BasisPos::setZ(double val) {
    _z = val;
}

void BasisPos::setOccupancy(double val) {
    if (val < 0 or val > 1) {
        throw std::runtime_error("BasisPos::setOccupancy() occupancy value must be between 0 and 1");
    }
    _occ = val;
}

void BasisPos::setXYZ(double x, double y, double z) {
    setX(x);
    setY(y);
    setZ(z);
}

void BasisPos::setLabel(std::string const& val) {
    _label = val;
}

void BasisPos::setTypeName(std::string const& val) {
    _type_name = val;
}

void BasisPos::setSpecies(chem::Element const* species) {
    _species = species;
}

void BasisPos::interpretSpeciesFromString(std::string const& species){
    _species = chem::str2symbol(species);
}

void BasisPos::setRadiusToAtomic() {
    _radius = species()->radii_atomic();
}

void BasisPos::setRadiusToCovalent() {
    _radius = species()->radii_covalent();
}

void BasisPos::setRadiusToMetallic() {
    _radius = species()->radii_metallic();
}

void BasisPos::setRadiusToVanDerWall() {
    _radius = species()->radii_van_der_wall();
}

void BasisPos::setRadiusType(AtomRadius type) {
    switch (type) {
    case AtomRadius::Atomic: setRadiusToAtomic(); break;
    case AtomRadius::VanDerWall: setRadiusToVanDerWall(); break;
    case AtomRadius::Covalent: setRadiusToCovalent(); break;
    case AtomRadius::Metallic: setRadiusToMetallic(); break;
    case AtomRadius::Manual: {}; break;
    }
}

void BasisPos::setRadius(double value) {
    if (value < 0.01 or value > 10) {
        throw std::runtime_error("BasisPos::setRadius(): Manual radius must be in the interval [0.01, 10]");
    }
    _radius = value;
}

void BasisPos::setRadiusScale(double val) {
    if (val == 0) {
        throw std::runtime_error("BasisPos::setRadiusScale(): Radius scale cannot be zero");
    }
    _radius_scale = val;

}



