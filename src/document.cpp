/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <document.h>
#include <iostream>
#include <cmath>
#include <elements.h>
#include <algorithm>
#include <geometry.h>
#include <ciso646>
#include <vtkobjects.h>
#include <math.h>
#include <helpers.h>


using help::isClose;

void Document::toDefault(){
    setA(5);
    setB(5);
    setC(5);
    setAlpha(90);
    setBeta(90);
    setGamma(90);
    setNx(1);
    setNy(1);
    setNz(1);
    setAtomPolygons(30);
    setBondPolygons(15);
    setSpaceGroupByName("1");
    setRadiusScale(0.6);
    setBondColor({0.57, 0.73, 0.81});
    setBondRadius(0.12);
    setDescr("");
    clearBases();
    clearBonds();
    setFilePath(QFileInfo(""));
    setBondStyle(BondStyle::Dual);
    setRadiusType(AtomRadius::Covalent);
    setAtomsVisible(true);
    setBondsVisible(true);
    setUnitCellVisible(true);
    setUnitCellLineWidth(1);
}

bool Document::setRadiusScale(double scale){
    if (scale <= 0){
        throw std::invalid_argument("scale factor cannot be <= 0");
    }
    else if (scale == _radius_scale) {
        return false;
    }
    _radius_scale = scale;

    // Give the basis positions a reference to the scaling factor
    for (auto& basis: basisPositions()) {
        basis->setRadiusScale(scale);
    }
    emit radiusScaleModified(scale);
    return true;
}

bool Document::setA(double val){
    if (val <= 0){
        throw std::invalid_argument("Lattice parameters cannot be <= 0");
    }
    else if (isClose(val, _a)) {
        return false;
    }
    this->_a = val;
    emit aModified(val);
    emit writableModified();
    return true;
}

bool Document::setB(double val){
    if (val <= 0){
        throw std::invalid_argument("Lattice parameters cannot be <= 0");
    }
    else if (isClose(val, _b)) {
        return false;
    }
    this->_b = val;
    emit bModified(val);
    emit writableModified();
    return true;
}

bool Document::setC(double val){
    if (val <= 0){
        throw std::invalid_argument("Lattice parameters cannot be <= 0");
    }
    else if (isClose(val, _c)) {
        return false;
    }
    this->_c = val;
    emit cModified(val);
    emit writableModified();
    return true;
}

bool Document::setAlpha(double val){
    if (val < 0 or val > 180){
        throw std::invalid_argument("Interaxial angles must be between 0 and 180 degrees");
    }
    else if (isClose(val, _alpha)) {
        return false;
    }
    this->_alpha = val;
    emit alphaModified(val);
    emit writableModified();
    return true;
}

bool Document::setBeta(double val){
    if (val < 0 or val > 180){
        throw std::invalid_argument("Interaxial angles must be between 0 and 180 degrees");
    }
    else if (isClose(val, _beta)) {
        return false;
    }
    this->_beta = val;
    emit betaModified(val);
    emit writableModified();
    return true;
}

bool Document::setGamma(double val){
    if (val < 0 or val > 180){
        throw std::invalid_argument("Interaxial angles must be between 0 and 180 degrees");
    }
    else if (isClose(val, _gamma)) {
        return false;
    }
    this->_gamma = val;
    emit gammaModified(val);
    emit writableModified();
    return true;
}

bool Document::setNx(unsigned int val) {
    if (val < 1) {
        throw std::runtime_error("Document::setNx() value must be greater than 0");
    }

    else if (val == _nx) {
        return false;
    }

    _nx = val;
    emit nxModified(val);
    return true;
}

bool Document::setNy(unsigned int val) {
    if (val < 1) {
        throw std::runtime_error("Document::setNy() value must be greater than 0");
    }

    else if (val == _ny) {
        return false;
    }

    _ny = val;
    emit nyModified(val);
    return true;
}

bool Document::setNz(unsigned int val) {
    if (val < 1) {
        throw std::runtime_error("Document::setNz() value must be greater than 0");
    }

    else if (val == _nz) {
        return false;
    }

    _nz = val;
    emit nzModified(val);
    return true;
}

bool Document::setSpaceGroup(gemmi::SpaceGroup const* spg){
    if (spg == nullptr){
        throw std::invalid_argument("Document::setSpaceGroup() space group cannot be null");
    }
    else if (spg == _spg) {
        return false;
    }
    _spg = spg;
    emit spaceGroupModified((QString::fromStdString(spg->xhm())));
    emit writableModified();
    return true;
}

bool Document::setSpaceGroupByName(std::string const& name){
    const gemmi::SpaceGroup* sg = gemmi::find_spacegroup_by_name(name);
    if (sg == nullptr) {
        throw std::invalid_argument("Document::setSpaceGroupByName() Space group designation is invalid");
    }
    else if (sg == _spg) {
        return false;
    }
    _spg = sg;
    emit spaceGroupModified((QString::fromStdString(name)));
    emit writableModified();
    return true;
}

void Document::addBasisPos(BasisPos const& newpos){
    _bases.push_back(std::make_unique<BasisPos>(std::move(newpos)));
    emit basisPosAdded(_bases.back().get());
    emit writableModified();
}

void Document::addBond(Bond const& newbond){
    _bonds.push_back(std::make_unique<Bond>(std::move(newbond)));
    emit bondAdded(_bonds.back().get());
}

std::vector<chem::Element const*> Document::elements() const {
    std::vector<chem::Element const*> retn {};
    for (auto const& i: basisPositions()){
        if (std::find(retn.begin(), retn.end(), i->species()) == retn.end()){
            retn.push_back(i->species());
        }
    }
    return retn;
}

unsigned int Document::nElemPos(chem::Element const* elem) const {
    unsigned int retn {0};
    for (auto const& base: basisPositions()) {
        if (base->species() == elem) {
            ++retn;
        }
    }
    return retn;
}

std::string Document::compStr() const {
    auto comp = composition();
    auto elems = elements();
    std::stringstream ss {};

    typedef std::pair<chem::Element const*, int> pair_t;
    std::vector<pair_t> data {};
    for (auto const& [elem, count]: comp) {
        data.push_back(std::make_pair(elem, count));
    }

    std::sort(data.begin(), data.end(), [](pair_t const& a, pair_t const& b){return a.first->number > b.first->number;});

    for (auto const& [elem, count]: data) {
        auto count_str = count == 1 ? "" : std::to_string(count);
        ss << elem->symbol + count_str;
    }

    return ss.str();
}

std::map<chem::Element const*, int> Document::composition() const {
    std::vector<int> counts {};

    std::vector<chem::Element const*> elems{};

    for (auto const& elem: elements()) {
        auto npos = nElemPos(elem);
        elems.push_back(elem);
        counts.push_back(npos);
    }

    int gcd = help::gcd(counts);

    std::map<chem::Element const*, int> retn {};

    for (int i=0; i<elems.size(); ++i) {
        retn[elems.at(i)] = counts.at(i) / gcd;
    }
    return retn;
}

std::map<BasisPos const*, Eigen::MatrixXd> Document::symEquivPos(bool include_boundary, bool tiled) const {
    std::map<BasisPos const*, std::vector<Vector3d>> temp {};
    std::map<BasisPos const*, Eigen::MatrixXd> retn;

    double x, y, z;

    // Generate positions for 1 unit cell
    for (auto const op: spaceGroup()->operations()){
        std::array<std::array<double, 4>, 4> seitz = op.float_seitz();

        for (auto const& pos: basisPositions()){
            x = pos->x();
            y = pos->y();
            z = pos->z();
            geom::applyInPlace(seitz, x, y, z);
            x = std::fmod((std::fmod(x, 1) + 1), 1);// + n_x;
            y = std::fmod((std::fmod(y, 1) + 1), 1);// + n_y;
            z = std::fmod((std::fmod(z, 1) + 1), 1);// + n_z;
            Vector3d row {};
            row << x, y, z;

            bool is_duplicate = false;
            for (auto const& curr_row: temp[pos.get()]) {
                if ((curr_row - row).cwiseAbs().sum() < 1e-6) {
                    is_duplicate = true;
                    break;
                }
            }

            if (not is_duplicate) {
                temp[pos.get()].push_back(std::move(row));
            }
        }
    }

    if (tiled) {
        // Generate positions for tiled unit cells
        for (auto& [base, positions]: temp) {
            std::vector<Vector3d> new_pos {};
            for (auto& pos: positions) {
                for (size_t n_x=0; n_x<nx(); ++n_x) {
                    for (size_t n_y=0; n_y<ny(); ++n_y) {
                        for (size_t n_z=0; n_z<nz(); ++n_z) {
                            x = pos(0) + n_x;
                            y = pos(1) + n_y;
                            z = pos(2) + n_z;
                            if (x >= 1 or y >= 1 or z >= 1) {
                                Vector3d coord {};
                                coord << x, y, z;
                                new_pos.push_back(coord);
                            }
                        }
                    }
                }
            }
            std::move(new_pos.begin(), new_pos.end(), std::back_inserter(positions));
        }
    }

    std::vector<std::vector<size_t>> offs {};

    if (include_boundary) {
        // Generate edge positions due to positions with indices = 0
        for (auto& [base, positions]: temp) {
            std::vector<Vector3d> new_pos {};
            for (auto& pos: positions) {
                x = pos[0];
                y = pos[1];
                z = pos[2];
                if (geom::isZero(x) or geom::isZero(y) or geom::isZero(z)) {
                    offs.clear();

                    // (0, y, z)
                    if (geom::isZero(x)) {
                        offs.push_back({nx(), 0, 0});
                    }

                    // (x, 0, z)
                    if (geom::isZero(y)) {
                        offs.push_back({0, ny(), 0});
                    }

                    // (x, y, 0)
                    if (geom::isZero(z)) {
                        offs.push_back({0, 0, nz()});
                    }

                    // (0, 0, z)
                    if (geom::isZero(x) and geom::isZero(y)) {
                        offs.push_back({nx(), ny(), 0});
                    }

                    // (0, y, 0)
                    if (geom::isZero(x) and geom::isZero(z)) {
                        offs.push_back({nx(), 0, nz()});
                    }

                    // (x, 0, 0)
                    if (geom::isZero(y) and geom::isZero(z)) {
                        offs.push_back({0, ny(), nz()});
                    }

                    // (0, 0, 0)
                    if (geom::isZero(x) and geom::isZero(y) and geom::isZero(z)) {
                        offs.push_back({nx(), ny(), nz()});
                    }

                    for (auto& dxyz: offs) {
                        Vector3d coord {};
                        coord << x+dxyz[0], y+dxyz[1], z+dxyz[2];
                        new_pos.push_back(coord);

                    }
                }
            }
            std::move(new_pos.begin(), new_pos.end(), std::back_inserter(positions));
        }
    }

    // Initilize keys and eigen matrices
    for (auto const& pair: temp){
        if (retn.count(pair.first) == 0){
            retn[pair.first] = Eigen::MatrixXd(pair.second.size(), 3);
        }
    }

    for (auto const& pair: temp){
        for (size_t i=0; i<pair.second.size(); ++i){
            retn[pair.first].row(i) = Eigen::VectorXd::Map(&pair.second[i][0], pair.second[i].size());
        }
    }
    return retn;
}


bool Document::setDescr(std::string const& text) {
    if (text == _descr) {
        return false;
    }

    _descr = text;

    emit descrModified(text);
    emit writableModified();
    return true;
}


bool Document::setAtomsVisible(bool val) {
    if (val != this->atomsVisible()) {
        _atoms_visible = val;
        emit atomsVisibleModified(val);
        return true;
    }
    return false;
}

bool Document::setBondsVisible(bool val) {
    if (val != this->bondsVisible()) {
        _bonds_visible = val;
        emit bondsVisibleModified(val);
        return true;
    }
    else {return false;}
}

bool Document::setUnitCellVisible(bool val) {
    if (val != this->unitCellVisible()) {
        _unitcell_visible = val;
        emit unitCellVisibleModified(val);
        return true;
    }
    return false;
}

bool Document::setUnitCellLineWidth(unsigned int val) {
    if (val < 1) {
        throw std::runtime_error("Document::setUnitCellLineWidth(): Line width cannot be less than 1");
    }
    else if (val == _unitcell_linewidth) {
        return false;
    }
    else {
        _unitcell_linewidth = val;
        emit unitCellLineWidthModified(val);
        return true;
    }
}

bool Document::setRadiusType(AtomRadius type) {
    if (radiusType() != type) {
        _radius_type = type;
    }
    else {return false;}

    for (auto const& pos: basisPositions()){
        pos->setRadiusType(type);
    }
    emit radiusTypeModified(type);
    return true;
}

bool Document::setBondStyle(BondStyle style) {
    if (_bond_style != style) {
        _bond_style = style;
        emit bondStyleModified(style);
        return true;
    }
    else {return false;}
}

bool Document::setAtomPolygons(unsigned int polys) {
    if (polys < 3){
        throw std::invalid_argument("Document::setAtomPolygons(): atom polygons can't be less than 3");
    }
    if (polys ==_atom_polygons) {
        return false;
    }
    _atom_polygons = polys;
    emit atomPolygonsModified(polys);
    return true;
}

bool Document::setBondPolygons(unsigned int polys) {
    if (polys < 3){
        throw std::invalid_argument("Document::setBondPolygons(): Bond polygons can't be less than 3");
    }
    if (polys == _bond_polygons) {
        return false;
    }
    _bond_polygons = polys;
    emit bondPolygonsModified(polys);
    return true;
}

bool Document::setBondRadius(double radius) {
    if (radius <= 0) {
        throw std::invalid_argument("Document::setBondRadius(): Bond radius cannot be <= 0");
    }
    if (isClose(radius,_bond_radius)) {
        return false;
    }
    _bond_radius = radius;
    emit bondRadiusModified(radius);
    return true;
}

bool Document::setBondColor(std::array<double, 3> color) {
    bool equal = true;
    for (size_t i=0; i<color.size(); ++i) {
        if (color.at(i) != _bond_color.at(i)) {
            equal = false;
            break;
        }
    }
    if (equal) {
        return false;
    }

    for (auto& i: color) {
        if (i < 0) {
            throw std::invalid_argument("Document::setBondColor(): Bond color values cannot be negative");
        }
    }
    _bond_color = color;
    emit bondColorModified(_bond_color);
    return true;
}

BasisPos* Document::findBasisPosById(unsigned int id) const {
    for (auto& pos: _bases){
        if (pos->id() == id){
            return pos.get();
        }
    }
    throw std::runtime_error("Document::findBasisById(): BasisPos of given id does not exist");
}

Bond* Document::findBondById(unsigned int id) const {
    for (auto& bond: _bonds){
        if (bond->id() == id){
            return bond.get();
        }
    }
    throw std::runtime_error("Document::findBondById(): Bond of given id does not exist");
}

void Document::removeBasisPosById(unsigned int id){
    for (size_t i=0; i<_bases.size(); ++i){
        if (_bases[i]->id() == id){
            _bases.erase(_bases.begin() + i);
            emit basisPosRemovedId(id);
            emit writableModified();

            return;
        }
    }
}

bool Document::basisHasWritableLabel() const {
    // If no bases have a label we must know this when writing cif file so as to not include the field
    for (auto const& base: basisPositions()) {
        QString label = QString::fromStdString(base->label());
        auto no_space = label.replace(" ", "");
        if (no_space.toStdString() != "") {
            return true;
        }
    }
    return false;
}

void Document::removeBondById(unsigned int id){
    for (size_t i=0; i<_bonds.size(); ++i){
        if (_bonds[i]->id() == id){
            _bonds.erase(_bonds.begin() + i);
            emit bondRemovedId(id);
            return;
        }
    }
}

void Document::removeElement(chem::Element const* elem){
    std::vector<unsigned int> remove_id {};
    for (auto& pos: _bases){
        if (pos->species() == elem){
            remove_id.push_back(pos->id());
        }
    }
    for (auto id: remove_id){
        removeBasisPosById(id);
    }
    emit elementRemoved(elem);
    emit writableModified();
}

void Document::clearBases(){
    _bases = std::vector<std::unique_ptr<BasisPos>> {};
    emit basesCleared();
    emit writableModified();
}

void Document::clearBonds(){
    _bonds = std::vector<std::unique_ptr<Bond>> {};
    emit bondsCleared();
}

unsigned int Document::nSymEquivPos() const {
    unsigned int count = 0;
    for (auto& [basis, pos]: symEquivPos()) {
        count += pos.rows();
    }
    return count;
}

std::array<double, 2> Document::predictBondingRadius(chem::Element const* elem1, chem::Element const* elem2){
    // Guess the min/max bonding range for 2 elements. This is determine by the sum of
    // the covalent radii and a tolerance facator
    double elem1_rad = elem1->radii_covalent();
    double elem2_rad = elem2->radii_covalent();
    double max_dist = elem1_rad + elem2_rad + bond_tol;
    double min_dist = elem1_rad + elem2_rad - bond_tol;
    return std::array<double, 2> {min_dist, max_dist};
}

void Document::predictBonding() {
    // Guess which bonds might be active and add them as Bonds
    clearBonds();
    auto elems = elements();
    for (size_t i=0; i<nElements(); ++i) {
        chem::Element const* elem1 = elems.at(i);
        for (size_t j=i; j<nElements(); ++j) {
            chem::Element const* elem2 = elems.at(j);

            double elneg_diff = fabs(elem1->elneg() - elem2->elneg());

            bool valid_bond = false;

            // Allow ionic bonds by electronegativity difference
            if (elneg_diff > 2){
                valid_bond = true;
            }
            // Allow bonding between two non-metals
            else if (elem1->elementType() == chem::ElementType::NonMetal and elem2->elementType() == chem::ElementType::NonMetal) {
                valid_bond = true;
            }
            // Allow bonding between two metalloids
            else if (elem1->elementType() == chem::ElementType::Metalloid and elem2->elementType() == chem::ElementType::Metalloid) {
                valid_bond = true;
            }
            // Allow bonding between a nonmetal and a metalloid
            else if ((elem1->elementType() == chem::ElementType::Metalloid and elem2->elementType() == chem::ElementType::NonMetal) or
                     ((elem2->elementType() == chem::ElementType::Metalloid and elem1->elementType() == chem::ElementType::NonMetal))){
                valid_bond = true;
            }
            // Allow bonding between a metal and a nonmetal
            else if ((elem1->isMetallic() and not elem2->isMetallic()) or
                     (elem2->isMetallic() and not elem1->isMetallic())){
                valid_bond = true;
            }

            // Do not allow Hydrogen-hydrogen bonding
            if (elem1->number == 1 and elem2->number ==1) {
                valid_bond = false;
            }

            if (not valid_bond) {
                continue;
            }

            Bond newbond {};
            newbond.setRemoveIfNoBonds(true);
            newbond.setElem1(elems.at(i));
            newbond.setElem2(elems.at(j));
            std::array<double, 2> bond_boundies = this->predictBondingRadius(elem1, elem2);
            newbond.setMaxRadius(bond_boundies.at(1));
            addBond(std::move(newbond));
        }
    }
}

std::vector<std::string> centeringString(gemmi::SpaceGroup const* spg){
    static std::map<char, std::vector<std::string>> data;
    data['F'] = {"0, 0, 0", "0, 1/2, 1/2", "1/2, 0, 1/2", "1/2, 1/2, 0"};
    data['I'] = {"0, 0, 0", "1/2, 1/2, 1/2"};
    data['A'] = {"0, 0, 0", "0, 1/2, 1/2"};
    data['B'] = {"0, 0, 0", "1/2, 0, 1/2"};
    data['C'] = {"0, 0, 0", "1/2, 1/2, 0"};
    data['P'] = {"0, 0, 0"};
    data['R'] = {"0, 0, 0", "1/3, 2/3, 2/3", "2/3, 1/3, 1/3"};
    std::string xhm = spg->xhm();
    return data[xhm[0]];
}

