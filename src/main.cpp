/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/
#include <mainwindow.h>
#include <QApplication>
#include <QSurfaceFormat>
#include "vtkAutoInit.h"
#include <QList>
#include <QString>
#include <QStringList>
#include <QSplashScreen>
#include <QTimer>
#include <QLabel>
#include <updatechecker.h>
#include <Eigen/Core>
#include <Eigen/Dense>
//#include <framelesshelpercore_global.h>
#include <framelesshelpercore_global.h>
#include <framelesshelperwidgets_global.h>
#include <FramelessHelper/Core/utils.h>
#include <Framelesshelper/Core/private/framelessconfig_p.h>
//#include <FramelessDialog>
//#include <framelessmainwindow.h>
//#include <framelessconfig_p.h>
#include <FramelessHelper/Core/framelesshelpercore_global.h>
//#include <FramelessWidgetHelper>
//#include <FramelessHelper>

using namespace wangwenx190;
//FRAMELESSHELPER_USE_NAMESPACE

Q_DECLARE_METATYPE(QStringList)

VTK_MODULE_INIT(vtkRenderingOpenGL2)
VTK_MODULE_INIT(vtkInteractionStyle)
VTK_MODULE_INIT(vtkRenderingFreeType)

int main(int argc, char** argv){
    FramelessHelper::FramelessHelper::Widgets::initialize();
    qRegisterMetaType<QStringList>();
    QApplication qapp(argc, argv);
    qapp.setWindowIcon(QIcon(":/images/icon.ico"));

    FramelessHelper::Utils::setDarkModeAllowedForApp(false);
    FramelessHelper::Utils::setQtDarkModeAwareEnabled(false);



    QApplication::setApplicationVersion(VERSION);
    QApplication::setOrganizationName("Arcte");
    QApplication::setOrganizationDomain("Arcte");
    QApplication::setApplicationName("Arcte");

    QSplashScreen splash(QPixmap(":images/images/image2.png"));
    splash.showMessage("Version " + qapp.applicationVersion() + "         Copyright (C) Jesse Smith 2022", Qt::AlignBottom);
//    splash.show();
    qapp.processEvents();

    QSurfaceFormat format = QVTKOpenGLStereoWidget::defaultFormat();
    format.setSamples(1);
    QSurfaceFormat::setDefaultFormat(format);

    // Check for updates
    UpdateChecker* updater = new UpdateChecker();
    updater->show_no_update_message = false;
    updater->run();

    // Adjust the styling on windows 10 for more modern appearance
    #ifdef WIN32
    QPalette pal = qapp.palette();
    QColor col = QColor(250, 250, 250);

    pal.setColor(QPalette::Window, col);
    pal.setColor(QPalette::Base, col);
    qapp.setPalette(pal);

//  Further adjust stylesheets
    std::stringstream ss {};
    ss << "QMenuBar {background: transparent; height: 25px}";
    ss << "QMenuBar::item:selected {background: rgb(235, 235, 235)}";
    ss << "QMenu {background: rgb(252, 252, 252); border: 1px solid rgb(200, 200, 200); border-radius: 6px; padding: 5px 5px 5px 5px; spacing: 20px}";
    ss << "QMenu::item {padding: 5px}";
    ss << "QMenu::separator {height: 1px; background: lightgray; margin-left: 25px; margin-right: 5px;}";
    ss << "QMenu::item:selected {background: rgb(242, 242, 242); color: black}";

    // Toolbars
//    ss << "QToolBar {background-color: rgb(252, 252, 252); padding: 3px; spacing: 3px}";
    ss << "QToolBar {background-color: rgb(245, 245, 245); padding: 3px; spacing: 3px}";
    ss << "QToolBar::separator {background: rgb(250, 250, 250); margin-left: 5px; margin-right: 5px;}";
    ss << "QToolBar { border-left-style: none; border-right-style: none;    border-bottom: 1px solid rgb(210, 210, 210); border-top: none}";

    // Dock widgets
    ss << "QDockWidget::title {background: transparent; margin-top:5px; text-align: center; padding-bottom: 3px}";

    // Groupbox
    ss << "QGroupBox {background: transparent}";

    // QComboBox
    ss << "QComboBox {background: rgb(245, 245, 245); border-style: solid; border-width: 1px; border-color: rgb(210, 210, 210); border-radius: 1px; padding: 2px 8px 2px 8px}";
    ss << "QComboBox::hover {background: #e5f3ff; border-color: #cce8ff}";
    ss << "QComboBox::down-arrow {image: url(:/icons/icons/downarrow.svg); width: 12px; height: 12px}";
    ss << "QComboBox::drop-down {border: 0px}";
    ss << "QComboBox QAbstractItemView {border: 1px solid rgb(210, 210, 210)}";

    // Spin boxes
    ss << "QSpinBox {padding: 1px 4px 2px 2px}";
    ss << "QDoubleSpinBox {border-color: blue; padding: 1px 4px 2px 2px}";

    // Checkbox
    ss << "QCheckBox::indicator {width: 17; height: 17}";

    // Views
    ss << "QTreeView {border: 1px solid rgb(210, 210, 210)}";
//    ss << "QAbstractItemView {border: 0px}";

    if (QSysInfo::productVersion() != "11") {
        // Pushbuttons
        ss << "QPushButton {background: rgb(242, 242, 242); border-style: solid; border-width: 1px; border-color: lightgrey; border-radius: 1px; padding: 4px 6px 4px 6px}";
        ss << "QPushButton::hover {background: #e5f3ff; border-color: #cce8ff}";
        ss << "QPushButton::checked {background: #cae6ff; border-color: #94ceff}";
        ss << "QPushButton::focus {border-color: #94ceff}";
        ss << "QPushButton::disabled {background: #d7d7d7; border-color: #cacaca}";
        ss << "QToolButton {padding: 2px 2px 2px 2px}";
    }

    qapp.setStyleSheet(QString::fromStdString(ss.str()));
    #endif

    MainWindow win;
    win.setAutoFillBackground(true);
    win.show();
    return qapp.exec();
}
