/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "modelexportworker.h"
#include <io.h>
#include <deque>
#include <vtkobjects.h>
#include <vtkAppendPolyData.h>
#include <vtkFillHolesFilter.h>
#include <vtkSTLWriter.h>
#include <ciso646>
#include <vtkOpenGLPolyDataMapper.h>

ModelExportWorker::ModelExportWorker(Document const* doc, std::vector<vtkSmartPointer<ArcteActor>> actors, QFileInfo filepath)
    :QThread(), doc(doc), actors(actors) {
    this->filepath = filepath;
    setTerminationEnabled(true);
}

void ModelExportWorker::run() {
    vtkSmartPointer<vtkPolyData> init1 = nullptr;
    vtkSmartPointer<vtkPolyData> init2 = nullptr;
    std::deque<vtkSmartPointer<vtkPolyData>> poly_atoms {};
    std::deque<vtkSmartPointer<vtkPolyData>> poly_other {};

    for (size_t i=0; i<actors.size(); ++i) {
        vtkSmartPointer<vtkActor> curr_actor = actors[i];
        auto mapper = vtkOpenGLPolyDataMapper::SafeDownCast(curr_actor->GetMapper());
        auto curr_data = mapper->GetInput();
        vtkSmartPointer<ArcteActor> aactor = ArcteActor::SafeDownCast(curr_actor);

        if (not aactor) {continue;}
        if (not io::actorIsBooleanable(aactor)) {continue;}

        std::string kind = aactor->kind;

        if (i == 0) {
            poly_atoms.push_back(curr_data);
            if (process()) {
                return;
            }
            continue;
        }

        if (kind == "bond") {
            poly_other.push_back(curr_data);
            continue;
        }

        bool intersect_found = false;
        for (auto& i: poly_atoms) {
            if (io::intersects(i, curr_data)) {
                i = vtkfactory::poly_union(i, curr_data, false);
                intersect_found = true;
                if (process()) {
                    return;
                }
                break;
            }

        }
        if (intersect_found == false) {
            poly_atoms.push_back(curr_data);
        }
    }

    vtkSmartPointer<vtkPolyData> final_data = std::move(poly_atoms[0]);
    poly_atoms.erase(poly_atoms.begin());

    while (true) {
        bool intersect_found = false;
        for (size_t i=0; i<poly_atoms.size(); ++i) {
            if (io::intersects(final_data, poly_atoms[i])) {
                final_data = vtkfactory::poly_union(final_data, poly_atoms[i], false);
                poly_atoms.erase(poly_atoms.begin()+i);
                intersect_found = true;
                if (process()) {
                    return;
                }
                break;
            }
        }
        if (intersect_found == false) {
            break;
        }
    }

    auto retn = vtkSmartPointer<vtkPolyData>::New();
    auto append = vtkSmartPointer<vtkAppendPolyData>::New();
    append->SetInputData(0, final_data);
    for (auto& data: poly_atoms) {
        append->AddInputData(data);
        if (process()) {
            return;
        }
    }
    append->Update();
    retn = append->GetOutput();

    for (size_t i=0; i<poly_other.size(); ++i) {
        retn = vtkfactory::poly_union(retn, poly_other[i], false);
        if (process()) {
            return;
        }
    }

    auto fill = vtkSmartPointer<vtkFillHolesFilter>::New();
    fill->SetInputData(retn);
    fill->SetHoleSize(0.1);
    fill->Update();

    emit update_count(actors.size());

    emit update_text(QString("Saving STL file..."));
    auto writer = vtkSmartPointer<vtkSTLWriter>::New();
    writer->SetFileName(filepath.absoluteFilePath().toStdString().data());
    writer->SetInputConnection(fill->GetOutputPort());
    writer->Write();
    emit update_text(QString("STL export successfully completed."));
}

ModelExportWorker::~ModelExportWorker() {
    quit();
    wait();
}

bool ModelExportWorker::process() {
    emit update_count(count);
    ++count;
    if (canceled) {
        emit update_text(QString("Canceling..."));
    }
    return canceled;
}
