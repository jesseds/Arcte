/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <zoneaxisdiff.h>
#include <geometry.h>
#include <scatteringfactors.h>
#include <math.h>
#include <QDebug>
#include <ciso646>
#include <iomanip>
#include <helpers.h>
#include <algorithm>

using namespace ediff;
using geom::pi;


QModelCombo::QModelCombo(QWidget* parent) : QComboBox(parent) {
    for (auto const& [text, kind]: model_labels) {
        addItem(QString::fromStdString(text));
    }
}


Model QModelCombo::currModel() {
    QString curr_text = currentText();

    for (auto const& [text, kind]: model_labels) {
        if (text == curr_text.toStdString()) {
            return kind;
        }
    }
    throw std::runtime_error("Could not map text to a valid model");
}


void QModelCombo::setModel(ediff::Model model) {

    for (auto const& [text, kind]: model_labels) {
        if (model == kind) {
            this->setCurrentText(QString::fromStdString(text));
            return;
        }
    }

    throw std::runtime_error("Could not map model to text label");
}


ZoneAxisPattern::ZoneAxisPattern(Document const* doc) : doc(doc) {
    // Setup reflections table
    _refl_table = new QTreeWidget();
    QStringList head_labels {
        "h",
        "k",
        "l",
        "Real Vg (V)",
        "Imag Vg (V)",
        "|Vg| (V)",
        "Rel int. (%)",
        "g (Å⁻¹)",
        "d (Å)",
        "Excitation (Å⁻¹)",
        "Laue zone"
    };
    _refl_table->setAlternatingRowColors(true);
    _refl_table->setHeaderLabels(head_labels);

    _refl_table->setIndentation(0);
    _refl_table->resizeColumnToContents(0);
    _refl_table->resizeColumnToContents(1);
    _refl_table->resizeColumnToContents(2);
    _refl_table->setSortingEnabled(true);
    for (int i=0; i<_refl_table->columnCount(); ++i) {
        _refl_table->resizeColumnToContents(i);
    }

    _refl_table->sortByColumn(7, Qt::AscendingOrder);

}


void ZoneAxisPattern::initialize() {
    setVoltage(200000);
    setZoneAxis(1, 0, 0);
    setMaxLaueZone(0);
    setMaxG(5);
    setMaxS(0.04);
    setThickness(500);
    setSfParam(sfs::Param::LobatoDyck);
    setModel(ediff::Model::Kinematical);
}


void ZoneAxisPattern::setZoneAxis(int u, int v, int w) {
    Vector3i zone {};
    zone << u, v, w;
    setZoneAxis(zone);
}


void ZoneAxisPattern::setZoneAxis(Vector3i const& value) {
    if (value.isApprox(_zone_axis)) {
        return;
    }

    _zone_axis = value;
    emit zoneAxisChanged(zoneAxis()(0), zoneAxis()(1), zoneAxis()(2));
}


void ZoneAxisPattern::setVoltage(double value) {
    if (help::isClose(value, - _voltage)) {
        return;
    }
    if (value < 0) {
        throw std::runtime_error("Voltage cannot be negative");
    }
    _voltage = value;
    emit voltageChanged(voltage());
}


void ZoneAxisPattern::setMaxLaueZone(int value) {
    if (value == _max_laue_zone) {
        return;
    }
    if (value < 0) {
        throw std::runtime_error("Laue zone cannot be negative");
    }

    _max_laue_zone = value;
    emit maxLaueZoneChanged(maxLaueZone());
}


void ZoneAxisPattern::setMaxG(double value) {
    if (help::isClose(value, - _max_g)) {
        return;
    }
    if (value < 0) {
        throw std::runtime_error("Max g cannot be negative");
    }
    _max_g = value;
    emit maxGChanged(maxG());
}


void ZoneAxisPattern::setMaxS(double value) {
    if (help::isClose(value, - _max_excitation)) {
        return;
    }
    if (value < 0) {
        throw std::runtime_error("Max excitation error cannot be negative");
    }
    _max_excitation = value;
    emit maxSChanged(maxS());
}


void ZoneAxisPattern::setThickness(double val) {
    if (help::isClose(val, _thickness)) {
        return;
    }

    if (val < 0) {
        throw std::runtime_error("thickness cannot be negative");
    }
    _thickness = val;

    emit thicknessChanged(val);
}

void ZoneAxisPattern::setSfParam(sfs::Param value) {
    if (value == _sf_param) {
        return;
    }
    _sf_param = value;
    emit sfParamChanged(value);
}


void ZoneAxisPattern::setModel(Model model) {
    if (_model == model) {
        return;
    }

    _model = model;
    emit modelChanged(model);
}


//std::pair<QStringList, std::vector<QTreeWidgetItem*>> const ZoneAxisPattern::refl_table () {
void ZoneAxisPattern::updateReflTable() {
    _refl_table->clear();

    int precision = 4;
    for (auto i=0; i<_hkls.rows(); ++i) {
        auto h = _hkls(i, 0);
        auto k = _hkls(i, 1);
        auto l = _hkls(i, 2);

        auto Freal = _Vg(i).real();
        auto Fimag = _Vg(i).imag();
        Fimag = geom::isZero(Fimag) ? 0 : Fimag;
        auto rel_int = _I(i) * 100;
        auto d = 1/_g(i);
        auto g = _g(i);
        auto norms = _Vg.array().rowwise().norm()(i);
        auto s = _excitation(i);
        auto laue = _laue(i);

        auto row = new QTreeWidgetItem{};
        row->setData(0, Qt::DisplayRole, h);
        row->setData(0, Qt::UserRole, h);

        row->setData(1, Qt::DisplayRole, k);
        row->setData(1, Qt::UserRole, k);

        row->setData(2, Qt::DisplayRole, l);
        row->setData(2, Qt::UserRole, l);

        row->setData(3, Qt::DisplayRole, QString::number(Freal, 'f', precision));
        row->setData(3, Qt::UserRole, Freal);

        row->setData(4, Qt::DisplayRole, QString::number(Fimag, 'f', precision));
        row->setData(4, Qt::UserRole, Fimag);

        row->setData(5, Qt::DisplayRole, QString::number(norms, 'f', precision));
        row->setData(5, Qt::UserRole, norms);

        row->setData(6, Qt::DisplayRole, QString::number(rel_int, 'f', precision));
        row->setData(6, Qt::UserRole, rel_int);

        row->setData(7, Qt::DisplayRole, QString::number(g, 'f', precision));
        row->setData(7, Qt::UserRole, g);

        row->setData(8, Qt::DisplayRole, QString::number(d, 'f', precision));
        row->setData(8, Qt::UserRole, d);

        row->setData(9, Qt::DisplayRole, QString::number(s, 'f', 6));
        row->setData(9, Qt::UserRole, s);

        row->setData(10, Qt::DisplayRole, laue);
        row->setData(10, Qt::UserRole, laue);

        _refl_table->addTopLevelItem(row);
    }
}


void ZoneAxisPattern::calculate() {
    if (not geom::validCellAngles(doc->alpha(), doc->beta(), doc->gamma())) {
        return;
    }
    if (not doc) {
        return;
    }

    double wl = wavelength();
    double g_max = maxG();

    MatrixXi iden = MatrixXi::Identity(3, 3);
    MatrixXd basis = geom::deorthogonalize(iden, doc->params(), doc->angles());
    Matrix3d mtensor = geom::metricTensor(doc->params(), doc->angles());
    Matrix3d deortho = geom::deorthogonalization(doc->params(), doc->angles());
//    Matrix3d r_deortho = geom::deorthogonalization(rparams, rangles);
    Matrix3d r_mtensor = mtensor.inverse();
    Matrix3d r_basis = (r_mtensor * basis).transpose();
    double vol = geom::volume(doc->params(), doc->angles());
//    r_basis.rowwise().normalize(dd);

    auto [hkls, g] = geom::hklsInShell(
                 doc->a(),
                 doc->b(),
                 doc->c(),
                 doc->alpha(),
                 doc->beta(),
                 doc->gamma(),
                 0,
                 g_max
                 );

    _hkls = std::move(hkls);
    _g = std::move(g);

    if (_hkls.rows() == 0) {
        return;
    }

    Eigen::Vector3d wave {};

    double rel_correction = geom::relElectronGamma(_voltage);
    wave = deortho * _zone_axis.cast<double>();

    // Calculate incident wave vector corrected for the mean inner potential
    double K0 = 1/wl;

    MatrixXi hkl000 {1, 3};
    hkl000 << 0, 0, 0;

    VectorXd g0 {1};
    g0 << 0;

    VectorXcd F000 = geom::structureFactors(*doc, hkl000, g0, _sf_param, rel_correction);
    VectorXcd U000 = geom::opticalPotential(F000, vol);
    K0 = std::sqrt(K0*K0 + U000(0).real());
    wave = wave.normalized() * K0;

    // Filter hkls in laue zone range
    _laue = (_hkls.array().rowwise() * _zone_axis.array().transpose()).rowwise().sum().abs();

    std::vector<long long> laue_filter {};
    for (long long i=0; i<_hkls.rows(); ++i) {
        if (_laue(i) <= _max_laue_zone) {
            laue_filter.push_back(i);
        }
    }

    help::rowwiseFilter(_laue, laue_filter);
    help::rowwiseFilter(_hkls, laue_filter);
    help::rowwiseFilter(_g, laue_filter);

    _hkls_g = (r_basis * _hkls.cast<double>().transpose()).transpose();


    // Filter hkls in excitation error range
    _excitation = geom::excitationError(wave, _hkls_g);
    std::vector<int> exc_filter {};
    double val = 0;
    for (auto i=0; i<_excitation.size(); ++i) {
        val = abs(_excitation(i));
        if (val < _max_excitation) {
            exc_filter.push_back(i);
        }
    }

    help::rowwiseFilter(_laue, exc_filter);
    help::rowwiseFilter(_hkls, exc_filter);
    help::rowwiseFilter(_hkls_g, exc_filter);
    help::rowwiseFilter(_g, exc_filter);
    help::rowwiseFilter(_excitation, exc_filter);

    _Fhkl = geom::structureFactors(*doc, _hkls, _g, _sf_param, rel_correction);
    _Vg = geom::crystalPotential(_Fhkl, vol);

    // Filter low intensity hkls
    _amplitude = (_Vg.array() * _Vg.array().conjugate()).real();

//    _I = _I / _I.maxCoeff();

    // Kinematical diffraction intensities
    if (_model == Model::Kinematical) {
        _I = _amplitude/_amplitude.maxCoeff();
//        _I = (_Fhkl.array() * _Fhkl.array().conjugate()).real();
        _I = _I/_I.maxCoeff();

        // Shape factor approximation
        double thick = 30; // Thickness value chosen for reasonable appearance
        Eigen::ArrayXd tmp = _excitation.array()*pi*thick;
        ArrayXd shape = (tmp.sin()/tmp).pow(2);
        _I = _I.array() * shape;
    }
    // Two-beam diffraction intensities
    if (_model == Model::TwoBeam) {
        ArrayXd xi = geom::extinctionDistance(_Fhkl, vol, wl);
        double t = _thickness;
        ArrayXd w = _excitation.array() * xi;
        ArrayXd s_eff = (w.square() + 1).sqrt()/xi;
        ArrayXd phi = (pi*_thickness/xi) * (pi*_thickness*s_eff).sin()/(pi*_thickness*s_eff);
        _I = 1/(1 + w.square()) * (pi*t/xi * (1+w.square()).sqrt());

        _I = phi.abs().square();
    }



    // Calculate relative intensities separately for each laue zone
    // Intialize each laue zone
    std::map<int, double> norms {};
    for (int i=0; i<= _max_laue_zone; ++i) {
        norms[i] = 0;
    }
    double amp = 0;
    double laue = 0;
    for (int i=0; i<_hkls.rows(); ++i) {
        amp = _I(i);
        laue = _laue(i);
        if (amp > norms[laue]) {
            norms[laue] = amp;
        }
    }

    for (int i=0; i<_hkls.rows(); ++i) {
        _I(i) = _I(i)/norms[_laue(i)];
    }




    std::vector<int> I_filter {};
    for (auto i=0; i<_hkls.rows(); ++i) {
        if (_I(i) > 1e-6) {
            I_filter.push_back(i);
        }
    }

    help::rowwiseFilter(_laue, I_filter);
    help::rowwiseFilter(_hkls, I_filter);
    help::rowwiseFilter(_Fhkl, I_filter);
    help::rowwiseFilter(_Vg, I_filter);
    help::rowwiseFilter(_hkls_g, I_filter);
    help::rowwiseFilter(_g, I_filter);
    help::rowwiseFilter(_amplitude, I_filter);
    help::rowwiseFilter(_I, I_filter);
    help::rowwiseFilter(_excitation, I_filter);

    // We have not generated the bragg angles yet
    _theta = geom::braggAngle(wl, 1/_g.array()); //degrees



    // -------------------- Project all g_hkl vectors onto the zone axis plane ----------------------------
    Vector3i up_idx {};
    up_idx << 0, 0, 1;

//    Vector3d up = geom::deorthogonalize(up_idx, rparams, rangles);
    Vector3d up = r_basis * up_idx.cast<double>();
    Vector3d up_norm = up.normalized();
    Vector3d wave_norm = wave.normalized();
    bool is_parallel = up_norm.isApprox(wave_norm, 1e-6) or up_norm.isApprox(wave_norm*-1, 1e-6);


    // If the up direction is parallel to the zone axis, choose an orthogonal up direction
    if (is_parallel) {
        up_idx(0) = 0;
        up_idx(1) = 1;
        up_idx(2) = 0;
        up = r_basis * up_idx.cast<double>();
    }

    Vector3d proj_x = up.cross(wave);
    proj_x.normalize();

    Vector3d proj_y = wave.cross(proj_x);
    proj_y.normalize();

    VectorXd new_y = (_hkls_g.array().rowwise() * proj_y.array().transpose()).rowwise().sum();
    VectorXd new_x = (_hkls_g.array().rowwise() * proj_x.array().transpose()).rowwise().sum();


    _projected = MatrixXd (_hkls.rows(), 2);
    _projected << std::move(new_x), std::move(new_y);

    VectorXd g2 = _hkls_g.rowwise().norm();

    updateReflTable();
    emit calculationFinished();

}
