/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <xrd.h>
#include <helpers.h>
#include <ciso646>
#include <complex>
#include <geometry.h>
#include <scatteringfactors.h>
#include <elements.h>

using help::isClose;
using namespace std::complex_literals;
using geom::pi;
using std::complex;
using namespace xrd;

// XrdPeak --------------------------------------------
std::string XrdPeak::label() const {
    std::stringstream ss {};
    ss << h << " " << k << " " << l << std::endl;
    return ss.str();}

// XrdSimulation -------------------------------------
SingleSimulation::SingleSimulation(Document const* doc) :
    QObject(),
    doc(doc) {
}

bool SingleSimulation::setWavelength(double wl) {
    if (wl < 0) {
        throw std::runtime_error("XrdSimulation::setWavelength(): Wavelength cannot be negative.");
    }
    if (isClose(wl, wavelength())) {
        return false;
    }
    else {
        _wavelength = wl;
        emit wavelengthModified(wl);
        return true;
    }
}

bool SingleSimulation::setRangeMin(double val) {
    if (val < 0) {
        throw std::runtime_error("XrdSimulation::setRangeMin(): Min range cannot be negative.");
    }
    if (isClose(_range_min, val)) {
        return false;
    }
    else {
        _range_min = val;
        emit rangeMinModified(val);
        return true;
    }
}

bool SingleSimulation::setRangeMax(double val) {
    if (isClose(_range_max, val)) {
        return false;
    }
    else {
        _range_max = val;
        emit rangeMaxModified(val);
        return true;
    }
}

void SingleSimulation::calculate() {
    if (rangeMin() > rangeMax()) {
        throw std::runtime_error("XrdSimulation::calculate(): Range max cannnot be less than the range min.");
    }
    else if (not geom::validCellAngles(doc->alpha(), doc->beta(), doc->gamma())) {
        return;
    }

    double thetamin = rangeMin()/2;
    double thetamax = rangeMax()/2;

    double wl = wavelength();

    double g_min = 1 / geom::braggDHkl(wl, thetamin, 1);  // 1/A
    double g_max = 1 / geom::braggDHkl(wl, thetamax, 1);  // 1/A

    auto data = geom::hklsInShell(
                 doc->a(),
                 doc->b(),
                 doc->c(),
                 doc->alpha(),
                 doc->beta(),
                 doc->gamma(),
                 g_min,
                 g_max
                 );
    auto n_hkl = data.first.rows();

    this->g = data.second;
    this->hkls = data.first;
    this->theta = geom::braggAngle(wl, 1/g.array().cast<double>(), 1); //degrees
    VectorXd theta_r = pi/180*theta; //radians


    // Structure factors
    this->Fhkl = geom::structureFactors(*doc, this->hkls, this->g, sfs::Param::Xray);

    // Compute the intensity
    this->intensity = (Fhkl.array() * Fhkl.array().conjugate()).real().abs();

    // Correct for lorentz polarization
    ArrayXd lp = ( (1 + pow(cos(2 * theta_r.array()), 2)) / (pow(sin(theta_r.array()), 2) * cos(theta_r.array())) );
    this->intensity.array() *= lp.array();

    // A scale factor may be provided
    this->intensity *= scale();

    // Condensed data per peak --------------------------------------
    _condensedPeaks.clear();
    std::stringstream th;
    th << std::fixed << setprecision(3);
    double it {0};
    for (size_t i=n_hkl; i-->0;) {
        th.str("");
        th << this->theta(i)*2;
        it = this->intensity(i);

        if (it < 1e-3) {
            continue;
        }

        if (_condensedPeaks.count(th.str()) == 0) {
            auto new_peak = std::make_unique<XrdPeak>();
            new_peak->h = this->hkls(i, 0);
            new_peak->k = this->hkls(i, 1);
            new_peak->l = this->hkls(i, 2);
            new_peak->g = this->g(i);
            new_peak->theta = this->theta(i);
            new_peak->I = it;
            new_peak->m = 1;
            new_peak->Fhkl_real = std::abs(Fhkl(i).real());
            new_peak->Fhkl_imag = std::abs(Fhkl(i).imag());
            _condensedPeaks[th.str()] = *new_peak;
            continue;
        }
        else {
            _condensedPeaks[th.str()].I += it;
            _condensedPeaks[th.str()].Fhkl_real += abs(Fhkl(i).real());
            _condensedPeaks[th.str()].Fhkl_imag += abs(Fhkl(i).imag());
            _condensedPeaks[th.str()].m += 1;
        }
    }

    // Normalize intensities to scale()
    double max_it = 0;
    for (auto const& peak: _condensedPeaks) {
        if (peak.second.I > max_it) {
            max_it = peak.second.I;
        }
    }

    for (auto& peak: _condensedPeaks) {
        peak.second.I *= scale()/max_it;
    }

    emit recalculated();
}

MultiSimulation::MultiSimulation(Document const* doc) : doc(doc) {
    setRangeMin(10);
    setRangeMax(90);

    // Setup reflections table
    _refl_table = new QTreeWidget();
    QStringList headers {
        "h",
        "k",
        "l",
        "λ",
        "Rel. int. (%)",
        "θ (°)",
        "2θ (°)",
        "d (Å)",
        "g (Å⁻¹)",
        "Σ|Real Fg|",
        "Σ|Imag Fg|",
        "Multiplicity"
    };
    _refl_table->setAlternatingRowColors(true);
    _refl_table->setHeaderLabels(headers);
    _refl_table->setIndentation(0);
    _refl_table->setSortingEnabled(true);
    _refl_table->sortByColumn(8, Qt::AscendingOrder);
    _refl_table->resizeColumnToContents(0);
    _refl_table->resizeColumnToContents(1);
    _refl_table->resizeColumnToContents(2);
    _refl_table->resizeColumnToContents(3);
//    _refl_table->setFrameStyle(QFrame::NoFrame);
}


void MultiSimulation::updateTable() {
    _refl_table->clear();
    auto count = 0;
    for (auto const& sim: allSims()) {
        if (sim->scale() < 1e-5) {
            ++count;
            continue;
        }
        int prec = 4;
        for (auto const& [theta, peak]: sim->condensedPeaks()) {
            QTreeWidgetItem* row = new QTreeWidgetItem();
            auto h = peak.h;
            auto k = peak.k;
            auto l = peak.l;
            auto lambda = count+1;
            auto intens =  peak.I*100;
            auto theta_b = peak.theta;
            auto g = peak.g;
            auto Freal = peak.Fhkl_real;
            auto Fimag = peak.Fhkl_imag;
            auto m = peak.m;

            row->setData(0, Qt::DisplayRole, h);
            row->setData(1, Qt::DisplayRole, k);
            row->setData(2, Qt::DisplayRole, l);
            row->setData(3, Qt::DisplayRole, lambda);

            row->setData(4, Qt::DisplayRole, QString::number(intens, 'f', prec));
            row->setData(4, Qt::UserRole, intens);

            row->setData(5, Qt::UserRole, theta_b);
            row->setData(5, Qt::DisplayRole, QString::number(theta_b, 'f', prec));

            row->setData(6, Qt::UserRole, theta_b*2);
            row->setData(6, Qt::DisplayRole, QString::number(theta_b*2, 'f', prec));

            row->setData(7, Qt::UserRole, 1/g);
            row->setData(7, Qt::DisplayRole, QString::number(1/g, 'f', prec));

            row->setData(8, Qt::UserRole, g);
            row->setData(8, Qt::DisplayRole, QString::number(g, 'f', prec));

            row->setData(9, Qt::DisplayRole, QString::number(Freal, 'f', prec));
            row->setData(9, Qt::UserRole, Freal);

            row->setData(10, Qt::DisplayRole, QString::number(Fimag, 'f', prec));
            row->setData(10, Qt::UserRole, Fimag);

            row->setData(11, Qt::DisplayRole, m);
            _refl_table->addTopLevelItem(row);
        }
        ++count;
    }
}


SingleSimulation* MultiSimulation::addSim(double wavelength, double scale) {
    auto new_sim = std::make_unique<SingleSimulation>(doc);
    new_sim->setWavelength(wavelength);
    new_sim->setScale(scale);
    new_sim->setRangeMin(rangeMin());
    new_sim->setRangeMax(rangeMax());
    _sims.push_back(std::move(new_sim));
    return _sims.at(nSims()-1).get();
}

void MultiSimulation::setRangeMin(double val) {
    _range_min = val;
    for (auto& sim: _sims) {
        sim->setRangeMin(val);
    }
}

void MultiSimulation::setRangeMax(double val) {
    _range_max = val;
    for (auto& sim: _sims) {
        sim->setRangeMax(val);
    }
}

void MultiSimulation::calculate() {
    for (auto& sim: _sims) {
        if (sim->scale() < 1e-5) {continue;}

        sim->calculate();
    }
    updateTable();
    emit calculated();
}








