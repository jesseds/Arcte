# Arcte
[![release](https://gitlab.com/jesseds/arcte/-/badges/release.svg?order_by=release_at)](https://gitlab.com/jesseds/arcte/-/releases)
[![pipeline status](https://gitlab.com/jesseds/arcte/badges/master/pipeline.svg?ignore_skipped=true)](https://gitlab.com/jesseds/arcte/-/commits/master)
[![License: GPL v2](https://img.shields.io/badge/License-GPL_v2-blue.svg)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)

![](https://gitlab.com/jesseds/arcte_meta/-/raw/master/out.png)

Arcte is an open-source application for visualization and analysis of crystallography and diffraction in crystalline solids.

General feature support:
- cif file read and write
- 3D interactive crystal structure visualization
- Atomic structure generation from 530 space group settings
- X-ray powder diffraction simulation (up to 3 wavelengths)
- Zone axis electron diffraction simulations in kinematical and 2-beam dynamical theories
- Bragg diffraction calculations
- Electron diffraction geometry calculations
- Direct- and reciprocal-space geometry calculations
- Tabulated X-ray structure factors
- Tabulated electron structure factors (Fg, Vg, Ug, and ξg) from multiple scattering factors sources
- Point-and-click bond length/angle measurement
- Export crystal structures as 3D model (stl mesh)

# Documentation

Documation can be found at https://jesseds.gitlab.io/arcte

# Download

1. Installers are currently only provided for Windows at https://gitlab.com/jesseds/arcte/-/releases
2. Otherwise Arcte must be compiled from source. Arcte is tested on Windows using MSVC 2019 and GCC in Linux. Thirdparty dependencies (Gemmi, Eigen) are provided in-source, although VTK >= 9.0 and Qt >= 5.12 must be separately linked.

   
