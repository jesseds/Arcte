/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once
#include <array>
#include <vector>
#include <string>
#include <map>
#include <elements.h>
#include <QComboBox>


namespace sfs {

enum class Param {
    DoyleTurner,
    Peng,
    Xray,
    LobatoDyck,
    MottBethe
};

constexpr const Param default_param = Param::LobatoDyck;

std::map<std::string, Param> paramLabels();


std::string param2label(sfs::Param);


// Base class for scattering factor types
class ScatteringFactors {

protected:
    std::map<std::string const, std::vector<double>> coef {};
    void addCoefficients(std::string elem, std::vector<double> coef);

public:
    bool exists(chem::Element const* elem) const;
    std::vector<double> coefficients(chem::Element const* elem) const;
    double virtual value(chem::Element const*, double q) const = 0;
};


// XRD scattering factors
class Xray : public ScatteringFactors {
private:
    void setup();

public:
    explicit Xray();
    double virtual value(const chem::Element *elem, double q) const;
};


// Doyle Turner electron diffraction scattering factors
class ElectronDoyleTurner : public ScatteringFactors {
private:
    void setup();

public:
    explicit ElectronDoyleTurner();
    double value(chem::Element const* elem, double g) const;
};


// Peng electron diffraction scattering factors
class ElectronPeng : public ScatteringFactors {
private:
    void setup();

public:
    explicit ElectronPeng();
    double value(chem::Element const* elem, double s) const;
};


// Mott-Bethe equation electron diffraction scattering factors
class ElectronMottBethe : public ScatteringFactors {
private:
    Xray _xray {};

public:
    explicit ElectronMottBethe(){}
    double value(chem::Element const* elem, double g) const;
};


// Lobato-Dyck electron diffraction scattering factors
class ElectronLobatoDyck : public ScatteringFactors {
private:
    void setup();

public:
    explicit ElectronLobatoDyck();
    double value(chem::Element const* elem, double s) const;
};


// Get scattering factors from Param enum
std::unique_ptr<ScatteringFactors> factorsFromEnum(Param kind);




// A combo box for selecting any scattering factors
class QScatteringFactorsCombo : public QComboBox {
    Q_OBJECT

private:
    Param _default_param = Param::LobatoDyck;
    bool _no_xray {};

    void setup();

public:
    QScatteringFactorsCombo(QWidget* parent);
    Param currentParam();

    void virtual setParam(Param);
};


// A combo box for selecting only electron scattering factors
class QScatteringFactorsElectronCombo : public QScatteringFactorsCombo {
    Q_OBJECT

private:
    Param _default_param = Param::Peng;

public:
    QScatteringFactorsElectronCombo(QWidget* parent);

    void setParam(Param) override;
};

}
