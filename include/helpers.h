/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once
#include <Eigen/Dense>


namespace help {

bool isClose(double val1, double val2, double diff=1e-5);


template<typename T1, typename T2>
void rowwiseFilter(Eigen::DenseBase<T1>& data, T2 const& idx) {
    T1 retn {idx.size(), data.cols()};

    int count = 0;
    for (auto const& row: idx) {
        for (auto col=0; col<data.cols(); ++col) {
            retn(count, col) = data(row, col);
        }
        ++count;
    }
    data = std::move(retn);
}

int gcd(int a, int b);

template<typename IT>
int gcd(IT first, IT last) {
    int result = *first;
    for (int i = 1; i < std::distance(first, last); i++) {
        result = gcd(first[i], result);

        if (result == 1) {
            return 1;
        }
    }

    return result;
}

template<typename T>
int gcd(T const& container) {
    return gcd(container.begin(), container.end());
}

}

