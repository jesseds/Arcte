/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once
#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsEllipseItem>
#include <QGraphicsSimpleTextItem>
#include <plotter.h>
#include <zoneaxisdiff.h>
#include <arctegraphicsitems.h>


// Class responsible for visualization of zone axis diffraction patterns
class ZoneAxisPlotter : public Plotter2d {
    Q_OBJECT

private:
    bool _inverted = false;

    ediff::ZoneAxisPattern* _pattern;
    static constexpr double _base_scale = 120;
    static constexpr double _base_cl = 300;
    QRectF _base_rect {};
    QTransform _base_transform {};

    double _camera_length;
    double _conv_semiangle;
    double _gamma;
    bool _show_spot_labels;
    bool _show_laue_circles {};
    bool _show_kikuchi_lines;
    double _hkl_label_thresh;
    double _kikuchi_thresh;
    double _rotation;

    QGraphicsSpotItem* makeSpot(Vector3i const& hkl, double gx, double gy, double I);
    QGraphicsSpotLabel* makeSpotLabel(Vector3i const& hkl, double gx, double gy);
    QGraphicsKikuchiItem* makeKikuchiLine(Vector3i const& hkl, double gx, double gy, double I);
    void reCenter();

public:
    explicit ZoneAxisPlotter(QWidget* parent, ediff::ZoneAxisPattern* pattern);
    void clear();
    void initializeProperties();

    auto convSemiAngle() const {return _conv_semiangle;}
    auto cameraLength() const {return _camera_length;}


public slots:
    void rebuild();

    void setCameraLength(double cl);
    void setConvSemiAngle(double angle);
    void setShowSpotLabels(bool val);
    void setGamma(double angle);
    void setShowLaueCircles(bool val);
    void setShowKikuchiLines(bool val);
    void setRotation(double val);
    void setHklLabelThreshold(double val);
    void setKikuchiThreshold(double val);

private slots:
    void onInvert();

signals:
    void convSemiAngleChanged(double val);
    void gammaChanged(double val);
    void showSpotLabelsChanged(bool val);
    void showLaueCirclesChanged(bool val);
    void rotationChanged(double val);
    void showKikuchiLinesChanged(bool val);
    void hklLabelThresholdChanged(double val);
    void kikuchiThresholdChanged(double val);
    void hoverTextChanged(QString txt);
};

