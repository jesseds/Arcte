/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include <elements.h>


class Bond {

private:
    chem::Element const* _elem_1;
    chem::Element const* _elem_2;
    double _min_rad {0.01};
    double _max_rad;
    static unsigned int next_id;
    unsigned int _id {0};
    bool _remove_if_no_bonds {false};

    void setId();

public:
    explicit Bond();

    unsigned int id() const {return _id;}
    chem::Element const* elem1() const {return _elem_1;}
    chem::Element const* elem2() const {return _elem_2;}
    double minRadius() const {return _min_rad;}
    double maxRadius() const {return _max_rad;}
    bool removeIfNoBonds() const {return _remove_if_no_bonds;}

    void setElem1(chem::Element const* elem) {_elem_1 = elem;}
    void setElem2(chem::Element const* elem) {_elem_2 = elem;}
    void setMinRadius(double rad);
    void setMaxRadius(double rad);

    // When the bond cylinders are created, this allows the bond to
    // be removed from the document if no bond connections were found
    void setRemoveIfNoBonds(bool val) {_remove_if_no_bonds = val;}
};

Q_DECLARE_METATYPE(Bond*)
