/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once
#include <document.h>
#include <geometry.h>
#include <scatteringfactors.h>
#include <QTreeWidgetItem>
#include <QStringList>

using namespace Eigen;

namespace ediff {


// Difference diffraction models (kinematical, 2-beam, Blochwave, etc)
enum class Model {
    Kinematical,
    TwoBeam
};


// List of UI strings for each model
std::array<std::pair<std::string const, Model>, 2> const model_labels {{
    {"Kinematical", Model::Kinematical},
    {"2-beam", Model::TwoBeam},
}};


// A QComboBox selection menu for each diffraction model
class QModelCombo : public QComboBox {
    Q_OBJECT

public:
    explicit QModelCombo(QWidget* parent);

    Model currModel();

public slots:
    void setModel(ediff::Model model);
};


// Primary class for calculating zone axis electron diffraction patterns
class ZoneAxisPattern : public QObject {
    Q_OBJECT

private:
    Document const* doc;
    VectorXd _g {};
    VectorXi _laue {};
    VectorXd _theta {};
    VectorXcd _Fhkl {};
    VectorXcd _Vg {};
    VectorXd _I {};
    VectorXd _amplitude {};
    MatrixXi _hkls {};
    MatrixXd _hkls_g {};
    VectorXd _excitation {};
    MatrixXd _projected {};

    // Initialization of these parameters are defered to initialize() to allow
    // listeners to update on initial value setting
    double _voltage; // volts
    Eigen::Vector3i _zone_axis;
    int _max_laue_zone;
    double _max_g;
    double _max_excitation;
    sfs::Param _sf_param;
    Model _model;
    double _thickness;

    QTreeWidget* _refl_table;
    void updateReflTable();

public:
    explicit ZoneAxisPattern(Document const* doc);

    // Initialize parameters (this is outside of constructure to allow listening widgets to update
    // on initial parameter definition
    void initialize();

    auto zoneAxis() const {return _zone_axis;}
    auto voltage() const {return _voltage;}
    auto g() const {return _g;}
    auto theta() const {return _theta;}
    auto Fhkl() const {return _Fhkl;}
    auto Vg() const {return _Vg;}
    auto hkls() const {return _hkls;}
    auto hklsG() const {return _hkls_g;}
    auto nHkls() const {return _hkls.rows();}
    auto excitation() const {return _excitation;}
    auto I() const {return _I;}
    auto amplitude () const {return _amplitude;}
    auto projected() const {return _projected;}
    auto maxG() const {return _max_g;}
    auto maxS() const {return _max_excitation;}
    auto laue() const {return _laue;}
    auto maxLaueZone() const {return _max_laue_zone;}
    auto sfParam() const {return _sf_param;}
    auto model() const {return _model;}
    auto thickness() const {return _thickness;}
    double wavelength() const {return geom::relElectronWl(voltage());}

    // Get data for filling a QTabulate of the reflection data
    QTreeWidget* refl_table() const {return _refl_table;}

public slots:
    void setZoneAxis(int u, int v, int w);
    void setZoneAxis(Eigen::Vector3i const& value);
    void setVoltage(double value);
    void setMaxLaueZone(int zone);
    void setMaxG(double value);
    void setMaxS(double value);
    void setSfParam(sfs::Param value);
    void setModel(ediff::Model model);
    void setThickness(double val);

    void calculate();

signals:
    void maxGChanged(double value);
    void maxSChanged(double value);
    void voltageChanged(double voltage);
    void sfParamChanged(sfs::Param);
    void maxLaueZoneChanged(unsigned int zone);
    void zoneAxisChanged(int h, int k, int l);
    void modelChanged(ediff::Model);
    void thicknessChanged(double val);
    void calculationFinished();
};

}
