/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include <QWidget>
#include <QtCharts/QChartView>
#include <QtCharts/QValueAxis>
#include <xrd.h>
#include <vector>
#include <QLineSeries>
#include <plotter.h>


class ArcteChartView : public QChartView {
    Q_OBJECT

public:
    explicit ArcteChartView() : QChartView() {}

protected:
    void virtual resizeEvent(QResizeEvent *event) override;

signals:
    void resized();
};


class XrdPlotter : public Plotter {
    Q_OBJECT

private:
    xrd::MultiSimulation const* xrd_sim;
    QChart* chart;
    ArcteChartView* _view;
    QValueAxis* xaxis;
    QValueAxis* yaxis;
    std::vector<QGraphicsTextItem*> labels {};
    void updateAxes();
    std::array<QPen, 3> _pens {};

public:
    explicit XrdPlotter(QWidget* parent, xrd::MultiSimulation const* xrd_simm);
    bool show_labels {true};
    bool show_d_spacing {false};
    bool show_2theta {false};
    double int_threshold = 0.025;
    void clear();
    void paint();

private slots:
    void onSetupLabels();

public slots:
    void onRedraw();
    void onShowLabelsChanged(bool);
    void onShowDSpacingChanged(bool);
    void onShow2ThetaChanged(bool);
    void onIntThresholdChanged(double);

};

