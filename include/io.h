/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include <QFileInfo>
#include <string>
#include <document.h>
#include <vtkBooleanOperationPolyDataFilter.h>
#include <vtkActorCollection.h>
#include <gemmi/cifdoc.hpp>
#include <QResource>

namespace io {

bool actorIsBooleanable(vtkSmartPointer<vtkActor> actor);
int getFirstBooleanableIdx(vtkSmartPointer<vtkActorCollection> actors);
bool intersects(vtkSmartPointer<vtkPolyData> poly1, vtkSmartPointer<vtkPolyData> poly2);
void load_cif_in_place(gemmi::cif::Document const& cif, Document* doc);
std::unique_ptr<Document> load_cif(QFileInfo const& cifpath);
std::unique_ptr<Document> load_cif(std::string const& cifpath);
void export_cif(QFileInfo const& cifpath, Document const& doc);

gemmi::cif::Document getCifDoc(QFileInfo const& path);

void export_union_stl(QFileInfo const& file, vtkSmartPointer<vtkActorCollection> actors, Document const* doc);
void export_append_stl(QFileInfo const& file, std::vector<vtkSmartPointer<ArcteActor>> actors);
}
