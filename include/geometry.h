/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once
#include <vector>
#include <stdexcept>
#include <algorithm>
#include <Eigen/Dense>
#include <document.h>
#include <gemmi/symmetry.hpp>
#include <scatteringfactors.h>


// This module contains common code paths for various geometrical and crystallographic computations
// All lengths are in angstroms except physical constants which are in standard units (meters, joules, etc)
// unless specifically specified. I.e. // a0 for the Bohr radius in meters and a0_ang for angstroms

using std::vector;
using Eigen::MatrixXd;
using Eigen::MatrixXi;
using Eigen::Matrix3d;
using Eigen::VectorXd;
using Eigen::Vector3d;
using Eigen::Vector3i;
using Eigen::VectorXcd;


typedef std::vector<std::vector<double>> vector2d;


namespace geom {

// Physical constants
constexpr const double pi = 3.1415926535;
constexpr const double m0 = 9.10938356e-31;  // Kg
constexpr const double ee = 1.6021766208e-19;  // Coulomb
constexpr const double h = 6.626070040e-34;  // J s
constexpr const double h2 = 4.135667662e-15;  // eV s
constexpr const double hbar = h/(2*pi);
constexpr const double hbar2 = h2/(2*pi);
constexpr const double c = 299792458.0;  // m/s
constexpr const double a0 = 5.291772109e-11; // Bohr radius (m)
constexpr const double a0_ang = 5.291772109e-1; // Bohr radius (m)
constexpr const double e0 = 8.8541878128e-12; // Vacuum permitivity (F / m)

// Convert degrees to radians
template <class T>
T inline deg2rad(T const& degrees){return pi/180.0*degrees;};

//Convert radians to degrees
template <class T>
T inline rad2deg(T const& radians){return 180.0/pi*radians;};

// Floored rounding to a given decimal
template <typename T>
T roundFloor(T val, unsigned int decimals = 3) {
    T multiplier = std::pow(10.0, decimals);
    return std::floor(val * multiplier) / multiplier;
}


Matrix3d metricTensor(double a, double b, double  c, double alpha, double beta, double  gamma);

template <typename T1, typename T2>
auto metricTensor(T1 const& params, T2 const& angles) {
    assert(params.size() == 3);
    assert(angles.size() == 3);
    return metricTensor(
                *params.begin(),
                *(params.begin()+1),
                *(params.begin()+2),
                *angles.begin(),
                *(angles.begin()+1),
                *(angles.begin()+2)
                );
}

Matrix3d deorthogonalization(double a, double b, double  c, double alpha, double beta, double  gamma);

template<typename T1, typename T2>
auto deorthogonalization(T1 const& params, T2 const& angles) {
    assert(params.size() == 3);
    assert(angles.size() == 3);
    return deorthogonalization(
                *params.begin(),
                *(params.begin()+1),
                *(params.begin()+2),
                *angles.begin(),
                *(angles.begin()+1),
                *(angles.begin()+2)
                );
}


Matrix3d deorthogonalization(Document const& strct);

template<typename T1, typename T2>
Vector3d deorthogonalize(Eigen::Vector3i const& vec, T1 const& params, T2 const& angles) {
    Matrix3d deortho = geom::deorthogonalization(params, angles);
    Eigen::Vector3d retn = deortho * vec.cast<double>();
    return retn;
}

// Deorthogonalize an array of vectors
template<typename T1, typename T2>
MatrixXd deorthogonalize(MatrixXi const& vecs, T1 const& params, T2 const& angles) {
    assert(params.size() == 3);
    assert(angles.size() == 3);
    Matrix3d deortho = geom::deorthogonalization(params, angles);
    MatrixXd retn = deortho * vecs.cast<double>().transpose();

    return retn.transpose();
}

std::array<double, 6> reciprocalParams(double a, double b, double c, double alpha, double beta, double gamma);

template <typename T1, typename T2>
auto reciprocalParams(T1 const& params, T2 const& angles) {
    assert(params.size() == 3);
    assert(angles.size() == 3);
    return reciprocalParams(
                *params.begin(),
                *(params.begin()+1),
                *(params.begin()+2),
                *angles.begin(),
                *(angles.begin()+1),
                *(angles.begin()+2)
                );
}


double volume(double a, double b, double c, double alpha, double beta, double gamma);
double volume(Document const& doc);

template<typename T1, typename T2>
auto volume(T1 const& params, T2 const& angles) {
    assert(params.size() == 3);
    assert(angles.size() == 3);
    return volume(
                *params.begin(),
                *(params.begin()+1),
                *(params.begin()+2),
                *angles.begin(),
                *(angles.begin()+1),
                *(angles.begin()+2)
                );
}
bool validCellAngles(double alpha, double beta, double gamma);

MatrixXd frac2cart(MatrixXd const& coords, Matrix3d const& deortho);
double uvwDist(Matrix3d mtensor, double u, double v, double w);
double hklDist(Matrix3d mtensor_r, double h, double k, double l);
double vecAngle(Matrix3d deortho, double u1, double v1, double w1, double u2, double v2, double w2);

bool isZero(double val, double epsilon=1e-5);
void applyInPlace(std::array<std::array<double, 4>, 4> const& seitz, double& x, double& y, double& z);// {

double braggAngle(double wl, double dhkl, int n=1);
VectorXd braggAngle(double wl, VectorXd const& dhkl, int n=1);
double braggWl(double angle, double dhkl, int n=1);
double braggDHkl(double wl, double angle, int n=1);

Matrix3d latticeVectors(double a, double b, double c, double alpha, double beta, double gamma);

template<typename T1, typename T2>
auto latticeVectors(T1 const& params, T2 const& angles) {
    assert(params.size() == 3);
    assert(angles.size() == 3);
    return latticeVectors(
                *params.begin(),
                *(params.begin()+1),
                *(params.begin()+2),
                *angles.begin(),
                *(angles.begin()+1),
                *(angles.begin()+2)
                );
}

std::pair<MatrixXi, VectorXd> hklsInShell(double a,
                   double b,
                   double c,
                   double alpha,
                   double beta,
                   double gamma,
                   double g_max,
                   double g_min);


Vector3i zoneAxis2(Vector3i const& hkl1, Vector3i const& hkl2);
bool zoneAxis3(Vector3i const& hkl1, Vector3i const& hkl2, Vector3i const& hkl3);

Eigen::VectorXcd structureFactors(Document const& doc, Eigen::MatrixXi const& hkls, Eigen::VectorXd const& g, sfs::Param scat_kind, double rel_corr=1);

VectorXcd crystalPotential(VectorXcd const& Fhkl, double volume);
VectorXcd opticalPotential(VectorXcd const& Fhkl, double volume);
VectorXd extinctionDistance(VectorXcd const& Fhkl, double volume, double wl);

VectorXd excitationError(Vector3d const& wave_vector, MatrixXd const& hkl_g);

// Kt is the component of the incident beam lying in the ZOLZ (this is zero for on-axis conditions)
//double laue_radius(int order, double g_zone, double wl, double Kt = 0);

double relElectronWl(double volts);
double relElectronGamma(double volts);
double relElectronMass(double volts);
std::array<int, 4> uvw2uvtw(int u, int v, int w);

template <class T>
int gcd(T const& vals) {
    int retn = 1;
    for (auto val = vals.begin(); val != vals.end(); ++val) {
        if (val == vals.begin()) {
            retn = *val;
            continue;
        }
        retn = std::gcd(retn, *val);
    }
    return retn;
}


}
