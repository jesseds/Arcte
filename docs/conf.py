# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import re
from pathlib import Path


cmake_lists = Path(os.path.abspath(os.path.dirname(__file__))) / ".." / "CMakeLists.txt"

with open(cmake_lists, "r") as file:
    txt = file.read()
    major = re.findall(r'"MAJOR_VERSION".*"(\d+)"', txt)
    minor = re.findall(r'"MINOR_VERSION".*"(\d+)"', txt)
    patch = re.findall(r'"PATCH_VERSION".*"(\d+)"', txt)

    assert len(major) == 1, "Found more than 1 major version match"
    assert len(minor) == 1, "Found more than 1 minor version match"
    assert len(patch) == 1, "Found more than 1 patch version match"

    major = major[0]
    minor = minor[0]
    patch = patch[0]

    version_list = (major, minor, patch)
    version_str = f"{major}.{minor}.{patch}"

    print(f"Arcte version is", version_str)


# -- Project information -----------------------------------------------------

project = "Arcte"
copyright = "2019-2023, Jesse Smith"
author = "Jesse Smith"

# The full version, including alpha/beta/rc tags
release = version_str


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx_sitemap",
    # "sphinx_rtd"
]

# Add any paths that contain templates here, relative to this directory.
# templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", "venv", ".idea"]

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = "sphinx_material"
# html_theme = "sphinx_rtd_theme"
html_theme_options = {
    "globaltoc_depth": 1,
    "repo_name": "Arcte on GitLab",
    "nav_title": "Arcte",
    "color_primary": "red",
    "color_accent": "orange",
    "globaltoc_collapse": False,
    "repo_type": "gitlab",
    "master_doc": True,
    "repo_url": "https://gitlab.com/jesseds/arcte",
    "base_url": "https://jesseds.gitlab.io/arcte/",
}

# html_static_path = ['_static']
html_favicon = "_static/favicon.svg"
html_logo = "_static/logomaterial.svg"
pygments_style = "sphinx"
html_sidebars = {"**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]}
html_extra_path = ['google9a0e44d229763c5f.html', "robots.txt"]
html_baseurl = "https://jesseds.gitlab.io/arcte"
sitemap_url_scheme = "{link}"
