Arcte: Interactive crystallography & diffraction
================================================

**Arcte is open-source software for interactive crystallography and diffraction of crystals.**

.. image:: _static/arcte2.png
    :alt: Arcte application window showing
    :align: center
    :width: 100%

Download & Installation
-----------------------
* Installers are currently only provided for Windows at https://gitlab.com/jesseds/arcte/-/releases
* For Linux, Arcte must be compiled from source. Arcte is tested on Windows using MSVC 2019 and GCC in Linux. Most third-party dependencies are provided in-source, however, VTK >= 9.0 and Qt >= 6 must be independently linked.

3D Interactivity
----------------

.. image:: _static/interactivity.png
    :alt: Interaction with crystals
    :align: center
    :width: 80%

Features:

* Mouse-based 3D interaction with crystal (pan, zoom, etc...)
* Set view orientation to vector
* Select and compute bond lengths and angles
* Save 3D model meshes, including 3D-printable (experimental)

Create or import structures
---------------------------

.. image:: _static/propertiesdock.svg
    :alt: Interaction with crystals
    :align: center
    :width: 90%

Features:

* Import and export cif files
* Interactively create structures composed of a unit cell, space group, basis vectors, elements, and bonds
* Tile unit cells in x, y, and z
* Adjustable element colors


Crystallographic calculations
-----------------------------

.. image:: _static/calculator.png
    :alt: Crystallographic calculations and vector geometry
    :align: center
    :width: 100%

Features:

* Real and reciprocal space vector geometry (angle, distance, dot, cross, and spacing)
* Real and reciprocal space unit cell geometry (parameters, angles, volume)
* Crystallography matrices (deorthogonalization matrix and real/reciprocal metric tensors)
* Bragg diffraction (Bragg angle, spacing/frequency, and wavelength)
* Zone axis geometry (Weiss zone law, zone axis rules, stage tilt span)
* Relativistic electron properties


Space groups
------------

.. image:: _static/spacegroups.png
    :alt: Selectable spacegroups window in Arcte
    :align: center
    :width: 80%

Features:

* 230 spacegroups
* 530 spacegroup settings
* Database from the Gemmi project


Structure factors
-----------------
.. image:: _static/structurefactors.png
    :alt: Window for calculation of structure factors
    :align: center
    :width: 80%

Features:

* Support X-ray and electron scattering factor calculations
* Multiple literature sources for electron scattering factors
* Adjustable hkl min/max
* Adjustable accelerating voltage and relativistic correction
* Exportable as csv or image

Electron diffraction
--------------------

.. image:: _static/electrondiffraction.png
    :alt: Window for simulation of zone axis electron diffraction patterns
    :align: center
    :width: 80%

Features:

* Simulate zone axis diffraction patterns
* Export diffraction pattern image and tabulated reflections
* Adjustable simulation parameters (accelerating voltage, zone axis, max Laue zone, and scattering factors)
* Adjustable appearance (camera length, conv. semi angle, rotation, hkl labels, Kikuchi lines)
* Tabulated list of reflections and metrics
* hkl labels filterable by spot intensity
* Support for Kinematical and 2-beam models (Blochwave in development)
* Diffraction spot-mouse interaction

X-ray diffraction
-----------------

.. image:: _static/x-raydiffraction.png
    :alt: Window for simulation of X-ray powder diffraction patterns
    :align: center
    :width: 100%

Features:

* Simulate kinematical X-ray powder diffraction patterns
* Tabulated list of reflections and their metrics
* Up to 3 wavelengths with adjustable weight
* Adjustable lower and upper 2-theta domain
* Filterable hkl labels by peak intensity
* Optional addition of d-spacing and 2-theta values to hkl labels
* Export diffraction pattern images (png) and table reflections (csv or png)



.. toctree::
   :maxdepth: 3
   :hidden:

   license
..
   getting_started
   install
   building_structs
   crystal_calc
   structure_factors
   xray_diffraction
   electron_diffraction
